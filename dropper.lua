local config = require("config")
local defConfig = { redstoneSide = "front", redstoneInverted = "false" }
local configFile = config.open("dropper.cfg", defConfig)
local rsSide = string.lower(configFile.redstoneSide or "front")
local rsInverted = configFile.redstoneInverted or false
local validSide = false

for _, side in ipairs(rs.getSides()) do
	rs.setOutput(side, false) -- Also reset states here
	if rsSide == side:lower() then validSide = true end
end

if not validSide then
	print("Invalid redstone side specified in config: " .. tostring(validSide))
	return
end

term.clear()
term.setCursorPos(1, 1)
print("I am dropper!")

turtle.select(1)
rs.setOutput(rsSide, rsInverted)

local e, a1
local offTimer
while true do
	e, a1 = os.pullEvent()
	
	if e == "turtle_inventory" then
		rs.setOutput(rsSide, not rsInverted)
		os.sleep(2)
		
		for i = 1, 16 do
			local item = turtle.getItemDetail(i)
			if item then
				turtle.select(i)
				turtle.dropDown(item.count)
				os.sleep(1)
			end
		end
		
		turtle.select(1)
		offTimer = os.startTimer(2.0)
	elseif e == "timer" and a1 == offTimer then
		rs.setOutput(rsSide, rsInverted)
	end
	
	os.sleep(0.05)
end