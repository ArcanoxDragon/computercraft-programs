term.clear()
term.setCursorPos(1, 1)
term.setCursorBlink(false)

local modem, mon

for _,v in pairs(rs.getSides()) do
	if peripheral.getType(v) == "modem" then
		modem = peripheral.wrap(v)
		break
	end
end

for _,v in pairs(rs.getSides()) do
	if peripheral.getType(v) == "monitor" then
		mon = peripheral.wrap(v)
		mon.setTextScale(0.5)
		break
	end
end

if not modem then
	print("No modem attached")
	return
end

if not mon then
	print("No monitor attached")
	return
end

local function split(text, delimiter)
	local resTable, lastLocation
	resTable = {}
	lastLocation = 0
	if #text == 1 then return {text} end
	while true do
		local l = string.find(text, delimiter, lastLocation, true)
		if l ~= nil then
			table.insert(resTable, string.sub(text, lastLocation, l - 1))
			lastLocation = l + 1
		else
			table.insert(resTable, string.sub(text, lastLocation))
			break
		end
	end
	return resTable
end

local function handleCommand(cmd)
	local cArry = split(cmd, ":")
	if #cArry == 0 then
		error("CRITICAL NETWORK ERROR. TERMINATING.")
	end
	local cmdName = cArry[1]
	if cmdName == "WRITE" then
		if #cArry < 2 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local msg = ""
		for i = 2, #cArry, 1 do
			msg = msg .. cArry[i]
			if i ~= #cArry then
				msg = msg .. " "
			end
		end
		mon.write(msg)
	elseif cmdName == "PRINT" then
		if #cArry ~= 2 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local msg = ""
		for i = 2, #cArry, 1 do
			msg = msg .. cArry[i]
			if i ~= #cArry then
				msg = msg .. " "
			end
		end
		mon.write(msg)
		mon.bumpLine()
	elseif cmdName == "CLEAR" then
		if #cArry ~= 1 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		mon.clear()
	elseif cmdName == "CLEARLINE" then
		if #cArry ~= 1 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		mon.clearLine()
	elseif cmdName == "SETCUR" then
		if #cArry ~= 3 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local x, y = tonumber(cArry[2]), tonumber(cArry[3])
		mon.setCursorPos(x, y)
	elseif cmdName == "BLINK" then
		if #cArry ~= 2 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local blink = tonumber(cArry[2])
		mon.setCursorBlink(blink > 0)
	elseif cmdName == "SCROLL" then
		if #cArry ~= 2 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local lines = tonumber(cArry[2])
		mon.scroll(lines)
	elseif cmdName == "TCOLOR" then
		if #cArry ~= 2 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local color = tonumber(cArry[2])
		if mon.isColor() then
			mon.setTextColor(color)
		end
	elseif cmdName == "BCOLOR" then
		if #cArry ~= 2 then
			error("CRITICAL NETWORK ERROR. TERMINATING.")
		end
		local color = tonumber(cArry[2])
		if mon.isColor() then
			mon.setBackgroundColor(color)
		end
	end
end

modem.open(os.getComputerID())
local cID = -1

while true do
	local e, side, sChannel, rChannel, msg, sDist = os.pullEvent("modem_message")
	if msg == "HI!" and rChannel ~= os.getComputerID() then
		local isColor = (term.isColor() and "1" or "0")
		local w, h = term.getSize()
		modem.transmit(rChannel, os.getComputerID(), "WELCOME:" .. isColor .. ":" .. tostring(w) .. ":" .. tostring(h))
		cID = rChannel
		print("Computer #" .. tostring(rChannel) .. " connected to me")
		mon.clear()
		mon.setCursorPos(1, 1)
		mon.setTextScale(1.3)
	elseif rChannel == cID and rChannel ~= os.getComputerID() then
		if msg == "BYE!" then
			print("Computer #" .. tostring(cID) .. " disconnected")
			mon.clear()
			mon.setCursorPos(1, 1)
			cID = -1
		else
			print("Received command '" .. msg .. "'")
			handleCommand(msg)
		end
	end
end