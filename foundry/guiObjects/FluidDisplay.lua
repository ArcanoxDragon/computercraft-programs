-- Prerequisites
local basalt = require("basalt")
local utils = require("foundryUtils")

-- Module locals
local bindMethod = utils.bindMethod
local findIndex = utils.findIndex
local formatFluidQuantity = utils.formatFluidQuantity
local floor = math.floor
local min = math.min
local max = math.max

-- FluidDisplay
local FluidDisplay = {}
FluidDisplay.__index = FluidDisplay

function FluidDisplay.new(parent)
	local object = {
		list = nil,
	}
	setmetatable(object, FluidDisplay)

	local frame = parent:addFrame()
		:setSize("{parent.w}", "{parent.h}")
		:setBackground(basalt.getTheme("ListBG"))

	object.list = frame:addList()
		:setSize("{parent.w - 3}", "{parent.h}")
		:setScrollable(true)
		:setSelectionColor(colors.cyan, colors.white)

	frame:addButton()
		:setText("/\\")
		:setTextHorizontalAlign("right")
		:setPosition("{parent.w - 1}", 1)
		:setSize(2, 3)
		:setBackground(colors.red)
		:onClick(bindMethod(object, object.onClickScrollUp))

	frame:addButton()
		:setText("\\/")
		:setTextHorizontalAlign("right")
		:setPosition("{parent.w - 1}", "{parent.h - 2}")
		:setSize(2, 3)
		:setBackground(colors.red)
		:onClick(bindMethod(object, object.onClickScrollDown))

	return object
end

function FluidDisplay:updateItems(fluids, filter)
	local selectedIndex = self.list:getItemIndex()
	local selectedItem = self.list:getItem(selectedIndex)
	local selectedFluid = selectedItem and selectedItem.args[1]

	self.list:clear()

	local quantityPlaceholder = "##### Buckets"
	local maxNameSize = self.list:getWidth() - quantityPlaceholder:len() - 2
	local itemFormat = ("%%-%ds  %%%ds"):format(maxNameSize, quantityPlaceholder:len()) -- not esoteric at all

	for _, fluid in pairs(fluids) do
		if type(filter) ~= "string" or filter == "" or fluid.name:lower():find(filter:lower(), nil, true) then
			local displayQuantity = formatFluidQuantity(fluid)
			local itemText = itemFormat:format(fluid.name, displayQuantity)

			self.list:addItem(itemText, nil, nil, fluid)
		end
	end

	self:selectFluid(selectedFluid)

	local maxOffset = max(0, self.list:getItemCount() - self.list:getHeight())
	local newOffset = min(maxOffset, self.list:getOffset())

	self.list:setOffset(newOffset)
end

function FluidDisplay:selectFluid(fluid)
	local fluidIndex = fluid and findIndex(self.list:getOptions(), function(item)
		local itemFluid = item.args[1]

		return itemFluid.id == fluid.id
	end) or nil

	self.list:selectItem(fluidIndex)
end

function FluidDisplay:onClickScrollUp()
	local scrollDelta = max(1, floor(self.list:getHeight() / 2 + 0.5))
	local newOffset = max(0, self.list:getOffset() - scrollDelta)

	self.list:setOffset(newOffset)
end

function FluidDisplay:onClickScrollDown()
	local scrollDelta = max(1, floor(self.list:getHeight() / 2 + 0.5))
	local maxOffset = max(0, self.list:getItemCount() - self.list:getHeight())
	local newOffset = min(maxOffset, self.list:getOffset() + scrollDelta)

	self.list:setOffset(newOffset)
end

return FluidDisplay
