-- CastingSelector
local CastingSelector = {}
CastingSelector.__index = CastingSelector

function CastingSelector.new(parent)
	local object = {
		frame = nil,
		fluid = nil,
		selectHandler = nil,
	}
	setmetatable(object, CastingSelector)

	local frame = parent:addFrame()
		:setSize("{parent.w}", "{parent.h}")
		:setPosition(1, 1)

	frame:addLabel("titleLabel")
		:setSize("{parent.w}", 1)
		:setText("No Fluid Selected")

	frame:addFlexbox("castingTypes")
		:setFlexDirection("column")
		:setSize("{parent.w}", "{parent.h - 2}")
		:setPosition(1, 3)

	object.frame = frame

	return object
end

function CastingSelector:show()
	self.frame:show()
end

function CastingSelector:hide()
	self.frame:hide()
end

function CastingSelector:setFluid(fluid)
	local this = self
	local titleLabel = self.frame:getChild("titleLabel")
	local castingTypesFrame = self.frame:getChild("castingTypes")

	castingTypesFrame:removeChildren()

	local fluidType = fluid and fluid.type
	local materialType = fluidType and fluidType.materialType
	local castingTypes = materialType and materialType.castingTypes

	if castingTypes then
		titleLabel:setText(("Cast %s into:"):format(fluid.name))

		for _, ct in ipairs(castingTypes) do
			local canCast = fluid.available >= ct.quantity
			local buttonText = ct.displayName .. "s" -- Show plural on button
			local button = castingTypesFrame:addButton()
				:setText(buttonText)
				:setSize("{parent.w}", 3)
				:onClick(function()
					this:select(ct)
				end)

			if not canCast then
				button:setBackground(colors.gray):setForeground(colors.lightGray):disable()
			end
		end
	elseif fluid then
		titleLabel:setText(("Cannot cast %s"):format(fluid.name))
	else
		titleLabel:setText("No fluid selected")
	end
end

function CastingSelector:onSelect(handler)
	if handler ~= nil and type(handler) ~= "function" then
		error("Handler must be a function")
	end

	self.selectHandler = handler
end

function CastingSelector:select(castingType)
	if type(self.selectHandler) == "function" then
		self.selectHandler(self, castingType)
	end
end

return CastingSelector
