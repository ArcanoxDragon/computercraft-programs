-- Prerequisites
local basalt = require("basalt")

-- Numpad
local Numpad = {}
Numpad.__index = Numpad

function Numpad.new(parent)
	local object = {
		frame = nil,
		valueLabel = nil,
		value = 0,
		submitHandler = nil,
		cancelHandler = nil,
	}
	setmetatable(object, Numpad)

	object.frame = parent:addFrame()
		:setSize("{parent.w}", "{parent.h}")
		:setPosition(1, 1)

	object.frame:addLabel("titleLabel")
		:setText("Enter value:")
		:setSize("{parent.w}", 1)
		:setPosition(1, 1)

	object.frame:addLabel("valueLabel")
		:setText("0")
		:setSize("{parent.w}", 1)
		:setPosition(1, 2)
		:setBackground(basalt.getTheme("InputBG"))
		:setForeground(basalt.getTheme("InputText"))

	local innerFrame = object.frame:addFrame("innerFrame")
		:setSize("{parent.w}", "{parent.h - 3}")
		:setPosition(1, 4)

	local function addDigitButton(digit)
		return innerFrame:addButton()
			:setSize("{(parent.w - 2) / 3}", 3)
			:setText(tostring(digit))
			:onClick(function()
				object:typeDigit(digit)
			end)
	end

	addDigitButton(1)
	addDigitButton(2):setPosition("{round((parent.w - 2) / 3) + 2}", 1)
	addDigitButton(3):setPosition("{2 * round((parent.w - 2) / 3) + 3}", 1)
	addDigitButton(4):setPosition(1, 5)
	addDigitButton(5):setPosition("{round((parent.w - 2) / 3) + 2}", 5)
	addDigitButton(6):setPosition("{2 * round((parent.w - 2) / 3) + 3}", 5)
	addDigitButton(7):setPosition(1, 9)
	addDigitButton(8):setPosition("{round((parent.w - 2) / 3) + 2}", 9)
	addDigitButton(9):setPosition("{2 * round((parent.w - 2) / 3) + 3}", 9)
	addDigitButton(0):setPosition(1, 13):setWidth("{2 * round((parent.w - 2) / 3) + 1}")

	-- Clear button
	innerFrame:addButton()
		:setSize("{(parent.w - 2) / 3}", 3)
		:setPosition("{2 * round((parent.w - 2) / 3) + 3}", 13)
		:setText("C")
		:onClick(function()
			object:setValue(0)
		end)

	-- Submit button
	innerFrame:addButton("submitButton")
		:setSize("{1.5 * round((parent.w - 2) / 3) + 1}", 3) -- Should end up half the size of the digit buttons
		:setPosition(1, 17)
		:setText("OK")
		:setBackground(colors.green)
		:onClick(function()
			object:submit()
		end)

	-- Cancel button
	innerFrame:addButton("cancelButton")
		:setSize("{(3 * round((parent.w - 2) / 3) + 2) - submitButton.w - 1}", 3)
		:setPosition("{submitButton.w + 2}", 17)
		:setText("Cancel")
		:setBackground(colors.red)
		:onClick(function()
			object:cancel()
		end)

	return object
end

function Numpad:show(keepValue)
	self.frame:show()

	if not keepValue then
		self:setValue(0)
	end
end

function Numpad:hide()
	self.frame:hide()
end

function Numpad:setTitle(title)
	self.frame:getChild("titleLabel"):setText(title)
end

function Numpad:typeDigit(digit)
	self:setValue(self.value * 10 + digit)
end

function Numpad:setValue(value)
	self.value = value
	self:updateFromValue()
end

function Numpad:setSubmitButtonText(text)
	self.frame:getChild("innerFrame"):getChild("submitButton"):setText(text)
end

function Numpad:updateFromValue()
	self.frame:getChild("valueLabel"):setText(tostring(self.value))
end

function Numpad:onSubmit(handler)
	if handler ~= nil and type(handler) ~= "function" then
		error("Handler must be a function")
	end

	self.submitHandler = handler
end

function Numpad:onCancel(handler)
	if handler ~= nil and type(handler) ~= "function" then
		error("Handler must be a function")
	end

	self.cancelHandler = handler
end

function Numpad:submit()
	if type(self.submitHandler) == "function" then
		self.submitHandler(self, self.value)
	end

	self:setValue(0)
end

function Numpad:cancel()
	if type(self.cancelHandler) == "function" then
		self.cancelHandler(self)
	end

	self:setValue(0)
end

return Numpad
