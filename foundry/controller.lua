-- Prerequisites
local FluidDb = require("FluidDb")

-- Module locals
local max = math.max

local FoundryController = {}
FoundryController.__index = FoundryController

function FoundryController.new(drain, tables, basins, outputInventory)
	local obj = {
		drain = drain,
		tables = tables,
		basins = basins,
		outputInventory = outputInventory,
		db = FluidDb.new(),
		fluids = {},
		castingQueue = {},
	}

	setmetatable(obj, FoundryController)

	return obj
end

function FoundryController:queueCasting(fluid, castingType, quantity)
	if not fluid or not castingType then
		error("Fluid and casting type are required")
	end
	if type(quantity) ~= "number" then
		error("Quantity must be a number")
	end

	if quantity < 0 then
		return false, "Quantity must be greater than 0"
	elseif quantity == 0 then
		-- Pointless, but no sense throwing an error
		return true
	end

	if #castingType.baseCastingType.castingTargets == 0 then
		return false, ("No targets found in which to cast %q"):format(castingType.displayName)
	end

	-- Ensure we get an up-to-date view of the requested fluid
	local queuedFluid = self:findFluid(fluid.id)

	if not queuedFluid then
		return false, ("Fluid not found in foundry: %s"):format(fluid.id)
	end

	local requestedFluidQuantity = quantity * castingType.quantity

	if queuedFluid.available < requestedFluidQuantity then
		return false,
			("Not enough %q in foundry (need %d mB, but only have %d mB)"):format(queuedFluid.name,
				requestedFluidQuantity, queuedFluid.available)
	end

	local queueItem = {
		fluid = queuedFluid,
		castingType = castingType,
		quantity = quantity,
	}

	table.insert(self.castingQueue, queueItem)
	return true
end

function FoundryController:update()
	self:updateFluids()
end

function FoundryController:process()
	self:processQueue()
	self:moveItemsToOutput()
end

function FoundryController:getReservedFluids()
	local reservedFluids = {}

	for _, v in ipairs(self.castingQueue) do
		local fluidId = v.fluid.id
		local reservedQuantity = v.quantity * v.castingType.quantity

		reservedFluids[fluidId] = (reservedFluids[fluidId] or 0) + reservedQuantity
	end

	return reservedFluids
end

function FoundryController:findFluid(id)
	for _, v in ipairs(self.fluids) do
		if v.id == id then
			return v
		end
	end

	return nil
end

function FoundryController:updateFluids()
	local reservedFluids = self:getReservedFluids()
	local fluids = {}

	for _, tank in pairs(self.drain.tanks()) do
		if tank then
			local fluidType = self.db:getFluidType(tank.name)
			local reserved = reservedFluids[tank.name] or 0
			local fluid = {
				id = tank.name,
				name = fluidType.name,
				amount = tank.amount,
				available = max(0, tank.amount - reserved),
				type = fluidType,
			}

			table.insert(fluids, fluid)
		end
	end

	table.sort(fluids, function(a, b)
		return a.name < b.name
	end)
	self.fluids = fluids
end

function FoundryController:processQueue()
	if #self.castingQueue == 0 then
		return
	end

	for _, operation in ipairs(self.castingQueue) do
		local castingType = operation.castingType
		local keepCasting = true

		while keepCasting and operation.quantity > 0 do
			local success = pcall(function()
				local castingTarget = self:findCastingTarget(castingType.baseCastingType)

				if castingTarget then
					-- Move specific quantity of fluid to casting target
					local fluidMoved = self.drain.pushFluid(
						peripheral.getName(castingTarget),
						castingType.quantity,
						operation.fluid.id)
					local didCast = fluidMoved > 0

					if didCast then
						operation.quantity = operation.quantity - 1
						os.sleep(0.1) -- Otherwise it seems we occasionally see the previous casting target in the next iteration
					end
				else
					keepCasting = false
				end
			end)

			if not success then
				keepCasting = false
			end
		end
	end

	self:pruneQueue()
end

function FoundryController:pruneQueue()
	local index = 1

	while index <= #self.castingQueue do
		-- If a queue entry has a quantity of 0, we remove it.
		-- Removing it shifts the table, so we only increment "index" if we do NOT remove an item.
		local entry = self.castingQueue[index]

		if entry.quantity == 0 then
			table.remove(self.castingQueue, index)
		else
			index = index + 1
		end
	end
end

function FoundryController:findCastingTarget(castingType)
	local castingTarget = nil

	for _, target in ipairs(castingType.castingTargets) do
		-- Available if there is no fluid in the target and the second inventory slot is empty
		local canTakeFluid = #target.tanks() == 0
		local hasRoomForItem = not target.list()[2]
		local canCast = canTakeFluid and hasRoomForItem

		if canCast then
			castingTarget = target
			break
		end
	end

	return castingTarget
end

function FoundryController:moveItemsToOutput()
	local targetName = peripheral.getName(self.outputInventory)

	for _, tbl in ipairs(self.tables) do
		local tableOutputStack = tbl.list()[2]

		if tableOutputStack and tableOutputStack.count and tableOutputStack.count > 0 then
			tbl.pushItems(targetName, 2)
		end
	end

	for _, basin in ipairs(self.basins) do
		local tableOutputStack = basin.list()[2]

		if tableOutputStack and tableOutputStack.count and tableOutputStack.count > 0 then
			basin.pushItems(targetName, 2)
		end
	end
end

function FoundryController:moveContentsToOutput(inventory)
end

return FoundryController
