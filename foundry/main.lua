-- Prerequisites
local config = require("config")
local FoundryController = require("controller")
local GuiHandler = require("gui")

-- Load config
local foundryConfig = config.load()

-- Peripherals
local drain = assert(table.pack(peripheral.find("tconstruct:drain"))[1], "Error: could not find a connected Drain")
local monitor = assert(table.pack(peripheral.find("monitor"))[1], "Error: could not find a connected Monitor")
local tables = { peripheral.find("tconstruct:table") }
local basins = { peripheral.find("tconstruct:basin") }
local outputInventory = peripheral.wrap(foundryConfig.outputInventory)

if not outputInventory then
	error(("Error: could not find output inventory %q"):format(foundryConfig.outputInventory))
end

-- Initialize
local controller = FoundryController.new(drain, tables, basins, outputInventory)
local gui = GuiHandler.new(monitor, controller)

-- Run
gui:run()
