-- Prerequisites
local log = require("basalt.libraries.basalt_logs")

-- Constants
local dataFile = "/foundry/data.json"

-- Module locals
local serializeJSON = textutils.serializeJSON
local unserializeJSON = textutils.unserializeJSON

-- Local functions
local function parseCastingType(castingType)
	local parsed = {
		displayName = castingType.displayName,
		castingTargets = {},
	}

	if type(castingType.castingTargets) == "table" then
		for _, t in ipairs(castingType.castingTargets) do
			local target = peripheral.wrap(t)

			if not target then
				error(("Casting target peripheral %q could not be found"):format(t))
			end

			table.insert(parsed.castingTargets, target)
		end
	end

	return parsed
end

local function parseMaterialType(castingTypes, materialType)
	local parsed = {
		castingTypes = {},
	}

	if type(materialType.castingTypes) == "table" then
		for _, t in ipairs(materialType.castingTypes) do
			local castingType = castingTypes[t.name]

			if not castingType then
				error(("Casting type not found: %s"):format(t.name))
			end

			local materialCastingType = {
				baseCastingType = castingType,
				displayName = t.displayName or castingType.displayName,
				quantity = t.quantity,
				maxDisplayQuantity = t.maxDisplayQuantity,
			}

			if type(t.quantity) == "number" and type(t.maxDisplayQuantity) == "number" then
				materialCastingType.maxDisplayQuantityMb = t.quantity * t.maxDisplayQuantity
			end

			table.insert(parsed.castingTypes, materialCastingType)
		end
	end

	return parsed
end

local function parseFluidType(materialTypes, fluidId, fluidType)
	local parsed = {
		id = fluidId,
		name = fluidType.name,
		materialType = nil,
	}

	if fluidType.materialType then
		parsed.materialType = materialTypes[fluidType.materialType]

		if not parsed.materialType then
			error(("Material type not found: %s"):format(fluidType.materialType))
		end
	end

	return parsed
end

local function parseRawData(rawData)
	local parsed = {
		castingTypes = {},
		materialTypes = {},
		fluidTypes = {},
	}

	if type(rawData.castingTypes) == "table" then
		for k, v in pairs(rawData.castingTypes) do
			parsed.castingTypes[k] = parseCastingType(v)
		end
	end

	if type(rawData.materialTypes) == "table" then
		for k, v in pairs(rawData.materialTypes) do
			parsed.materialTypes[k] = parseMaterialType(parsed.castingTypes, v)
		end
	end

	if type(rawData.fluidTypes) == "table" then
		for k, v in pairs(rawData.fluidTypes) do
			parsed.fluidTypes[k] = parseFluidType(parsed.materialTypes, k, v)
		end
	end

	return parsed
end

local FluidDb = {}
FluidDb.__index = FluidDb

function FluidDb.new()
	local obj = {
		rawData = {},
		data = {},
	}

	setmetatable(obj, FluidDb)
	obj:read()

	return obj
end

function FluidDb:read()
	local file = io.open(dataFile, "r")

	if not file then
		log(("Error: could not open file %q for reading"):format(dataFile))
		return
	end

	local jsonText = file:read("a")

	file:close()

	local rawData = unserializeJSON(jsonText)

	self.rawData = rawData
	self.data = parseRawData(rawData)
end

function FluidDb:write()
	local file = io.open(dataFile, "w")

	if not file then
		log(("Error: could not open file %q for writing"):format(dataFile))
		return
	end

	local jsonText = serializeJSON(self.rawData)

	file:write(jsonText)
	file:flush()
	file:close()
end

function FluidDb:getFluidType(id)
	local fluidType = self.data.fluidTypes[id]

	if not fluidType then
		-- Insert into raw data and re-parse
		self.rawData.fluidTypes = self.rawData.fluidTypes or {}
		self.rawData.fluidTypes[id] = { name = id }
		self.data = parseRawData(self.rawData)

		-- Save raw data back to file
		self:write()
	end

	-- Try again
	fluidType = self.data.fluidTypes[id]

	-- Should have found it this time, but if not, throw an error
	if not fluidType then
		error(("Could not find new fluid type %q after adding it"):format(id))
	end

	return fluidType
end

return FluidDb
