local log = require("basalt.libraries.basalt_logs")

local M = {}

-- Module locals
local floor = math.floor

function M.bindMethod(target, method)
	if type(method) ~= "function" then
		error("\"method\" must be a function")
	end

	return function(...)
		method(target, ...)
	end
end

function M.findIndex(table, predicate)
	if type(table) ~= "table" then
		error("\"table\" must be a table")
	end
	if type(predicate) ~= "function" then
		error("\"predicate\" must be a function")
	end

	for index, item in ipairs(table) do
		if predicate(item) then
			return index
		end
	end

	return nil
end

--#region Fluid Quantity Formatting

local MB_PER_BUCKET = 1000

local LONGEST_UNIT_LENGTH = ("Buckets"):len()
local QUANTITY_FORMAT = ("%%d %%-%ds"):format(LONGEST_UNIT_LENGTH)

function M.formatFluidQuantity(fluid, onlyAvailable)
	local amount = onlyAvailable == false and fluid.amount or fluid.available
	local unit = "mB"

	local materialType = fluid.type and fluid.type.materialType
	local castingTypes = materialType and materialType.castingTypes
	local castingType = nil

	if castingTypes then
		for _, ct in ipairs(castingTypes) do
			if not ct.maxDisplayQuantityMb or amount < ct.maxDisplayQuantityMb then
				castingType = ct
				break
			end
		end

		if not castingType then
			castingType = castingTypes[#castingTypes]
		end
	end

	if castingType then
		unit = castingType.displayName
		amount = floor(amount / castingType.quantity)
	elseif amount >= MB_PER_BUCKET then
		amount = floor(amount / MB_PER_BUCKET)
		unit = "Bucket"
	end

	if amount ~= 1 and unit ~= "mB" then
		unit = unit .. "s"
	end

	return QUANTITY_FORMAT:format(amount, unit)
end

--#endregion Fluid Quantity Formatting

return M
