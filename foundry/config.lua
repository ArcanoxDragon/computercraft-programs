local M = {}

-- Module locals
local unserializeJSON = textutils.unserializeJSON

-- Constants
local fileName = "/foundry/config.json"

-- Functions
function M.load()
	local configFile = io.open(fileName, "r")

	if not configFile then
		error(("Could not open %q for reading"):format(fileName))
	end

	local configJson = configFile:read("*a")

	configFile:close()

	return unserializeJSON(configJson) or error("Could not parse config file")
end

return M
