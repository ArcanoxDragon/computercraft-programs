-- Prerequisites
local basalt = require("basalt")
local basaltUtils = require("basalt.libraries.utils")
local utils = require("foundryUtils")
local CastingSelector = require("guiObjects.CastingSelector")
local FluidDisplay = require("guiObjects.FluidDisplay")
local Numpad = require("guiObjects.Numpad")

-- Module locals
local max = math.max
local bindMethod = utils.bindMethod

--#region GuiHandler

local GuiHandler = {}
GuiHandler.__index = GuiHandler

function GuiHandler.new(monitor, controller)
	local obj = {
		monitor = monitor,
		controller = controller,

		baseFrame = nil,
		updateThread = nil,
		processThread = nil,
		fluidDisplays = {},

		toastTimer = nil,
		toastLabels = {},

		searchText = "",
		searchInput = nil,
		inSearchChangedHandler = false,

		selectedFluid = nil,
		inFluidChangedHandler = false,

		castingType = nil,
		castingSelectors = {},
		quantityInput = nil,
		focusQuantityInputTimer = nil,
		numpad = nil,
	}

	setmetatable(obj, GuiHandler)
	return obj
end

function GuiHandler:run()
	-- Set up UI
	self:initTheme()
	self:initComputerScreen()
	self:initMonitor()

	-- Start threads
	self.updateThread = self.baseFrame:addThread()
	self.updateThread:start(bindMethod(self, self.updateThreadFunc))

	self.processThread = self.baseFrame:addThread()
	self.processThread:start(bindMethod(self, self.processThreadFunc))

	-- Start rendering UI
	self.baseFrame:show()
	basalt.autoUpdate()
end

function GuiHandler:updateThreadFunc()
	while true do
		self.controller:update()

		for _, display in pairs(self.fluidDisplays) do
			display:updateItems(self.controller.fluids, self.searchText)
		end

		sleep(0.5)
	end
end

function GuiHandler:processThreadFunc()
	while true do
		self.controller:process()
		sleep(0.5)
	end
end

function GuiHandler:initTheme()
	basalt.setTheme("BaseFrameBG", colors.black)
	basalt.setTheme("BaseFrameText", colors.lightGray)
	basalt.setTheme("FrameBG", colors.black)
	basalt.setTheme("FrameText", colors.lightGray)
	basalt.setTheme("ButtonBG", colors.cyan)
	basalt.setTheme("ButtonText", colors.white)
	basalt.setTheme("InputBG", colors.gray)
	basalt.setTheme("InputText", colors.white)
	basalt.setTheme("ListText", colors.white)
	basalt.setTheme("SelectionBG", colors.cyan)
end

function GuiHandler:initPalette(frame)
	frame:show() -- Ensure frame is the active frame
	frame:setPalette("gray", 0x222222)
	frame:setPalette("lightGray", 0x888888)
end

function GuiHandler:initComputerScreen()
	local baseFrame = basalt.createFrame()

	self:initPalette(baseFrame)

	-- Left half
	local leftFrame = baseFrame:addFrame("leftFrame")
		:setSize("{(parent.w - 1) * (2 / 3)}", "{parent.h}")

	leftFrame:addLabel()
		:setPosition(2, 2)
		:setText("Search Fluids:")

	local searchInput = leftFrame:addInput()
		:setPosition(2, 3)
		:setSize("{parent.w - 2}", 1)
		:onChange(bindMethod(self, self.onSearchChanged))
		:onClick(bindMethod(self, self.onSearchClicked))

	self:addFluidDisplay(leftFrame)
		:setPosition(2, 5)
		:setSize("{parent.w - 2}", "{parent.h - 5}")

	-- Right half
	local rightFrame = baseFrame:addFrame("rightFrame")
		:setSize("{parent.w - leftFrame.w}", "{parent.h}")
		:setPosition("{leftFrame.w + 1}", 1)

	self:addCastingSelector(rightFrame).frame
		:setSize("{parent.w - 2}", "{parent.h - 1}")
		:setPosition(2, 2)

	self:addKeyboardQuantityInput(rightFrame)
		:setSize("{parent.w - 2}", "{parent.h - 2}")
		:setPosition(2, 4)

	-- Common
	self.toastTimer = baseFrame:addTimer()
		:setTime(8)
		:onCall(bindMethod(self, self.onToastTimerCalled))

	self:addToastLabel(baseFrame)

	self.baseFrame = baseFrame
	self.searchInput = searchInput
end

function GuiHandler:initMonitor()
	if not self.monitor then
		return
	end

	self.monitor.setTextScale(0.5)

	local monitorFrame = basalt.addMonitor():setMonitor(self.monitor)

	self:initPalette(monitorFrame)

	-- Left half
	local leftFrame = monitorFrame:addFrame("leftFrame")
		:setSize("{(parent.w - 1) * (3 / 5)}", "{parent.h}")

	self:addFluidDisplay(leftFrame)
		:setSize("{parent.w}", "{parent.h}")

	-- Right half
	local rightFrame = monitorFrame:addFrame("rightFrame")
		:setSize("{parent.w - leftFrame.w}", "{parent.h}")
		:setPosition("{leftFrame.w + 1}", 1)

	local numpad = Numpad.new(rightFrame)

	numpad.frame:setSize("{parent.w - 2}", "{parent.h - 2}"):setPosition(2, 2)
	numpad:setSubmitButtonText("Cast")
	numpad:onCancel(function()
		self:selectCastingType(nil)
	end)
	numpad:onSubmit(function(_, value)
		self:confirmCasting(value)
	end)
	numpad:hide()

	self:addCastingSelector(rightFrame).frame
		:setSize("{parent.w - 2}", "{parent.h}")
		:setPosition(2, 1)

	-- Common
	self:addToastLabel(monitorFrame)

	self.numpad = numpad
end

function GuiHandler:addToastLabel(parent)
	local label = parent:addLabel()
		:setSize("{parent.w}", 1)
		:setPosition(1, "{parent.h - self.h + 1}")
		:setZ(10)
		:setBackground(colors.gray)

	label:hide()
	table.insert(self.toastLabels, label)
end

function GuiHandler:addFluidDisplay(parent)
	local frame = parent:addFrame()
	local display = FluidDisplay.new(frame)

	display.list:onSelect(bindMethod(self, self.onFluidSelectionChanged))

	table.insert(self.fluidDisplays, display)
	return frame
end

function GuiHandler:addCastingSelector(parent)
	local this = self
	local castingSelector = CastingSelector.new(parent)

	castingSelector:onSelect(function(_, castingType)
		this:selectCastingType(castingType)
	end)

	table.insert(self.castingSelectors, castingSelector)
	return castingSelector
end

function GuiHandler:addKeyboardQuantityInput(parent)
	local frame = parent:addFrame()
		:setSize("{parent.w}", "{parent.h}")

	frame:addLabel("titleLabel")
		:setText("Quantity:")
		:setSize("{parent.w}", 1)

	local input = frame:addInput("quantityInput")
		:setInputType("number")
		:setSize("{parent.w}", 1)
		:setPosition(1, 2)

	frame:addButton("submitButton")
		:setText("Cast")
		:setSize("{(parent.w - 1) / 2}", 3)
		:setPosition(1, 4)
		:setBackground(colors.green)
		:onClick(function()
			local quantity = tonumber(input:getValue())

			if type(quantity) == "number" then
				self:confirmCasting(quantity)
			end
		end)

	frame:addButton("cancelButton")
		:setText("Cancel")
		:setSize("{parent.w - submitButton.w - 1}", 3)
		:setPosition("{submitButton.w + 2}", 4)
		:setBackground(colors.red)
		:onClick(function()
			self:selectCastingType(nil)
		end)

	self.focusQuantityInputTimer = frame:addTimer()
		:setTime(0.1)
		:onCall(function()
			local e = input

			while e do
				e:setFocus()
				e = e:getParent()
			end
		end)

	frame:hide()
	self.quantityInput = frame
	return frame
end

function GuiHandler:showToast(toast, color)
	color = color or basalt.getTheme("LabelText")

	for _, label in ipairs(self.toastLabels) do
		local lines = basaltUtils.wrapText(toast, label:getWidth())

		label
			:show()
			:setHeight(max(0, #lines))
			:setText(toast)
			:setForeground(color)
	end

	self.toastTimer:cancel()
	self.toastTimer:start()
end

function GuiHandler:setSelectedFluid(fluid)
	if fluid ~= self.selectedFluid then
		self:selectCastingType(nil)
	end

	self.selectedFluid = fluid

	for _, display in ipairs(self.fluidDisplays) do
		display:selectFluid(fluid)
	end

	for _, selector in ipairs(self.castingSelectors) do
		selector:setFluid(fluid)
	end
end

function GuiHandler:setSearchText(value)
	self.searchText = value
	self.searchInput:setValue(value)
end

function GuiHandler:selectCastingType(castingType)
	self.castingType = castingType

	if castingType then
		local fluidName = self.selectedFluid and self.selectedFluid.name or "Unknown"
		local castingTypeText = castingType.displayName .. "s"
		local castingTitle = ("%s %s to cast:"):format(fluidName, castingTypeText)

		for _, v in ipairs(self.castingSelectors) do
			v:hide()
		end

		if self.quantityInput then
			self.quantityInput:show()
			self.quantityInput:getChild("quantityInput"):setValue("")
			self.quantityInput:getChild("titleLabel"):setText(castingTitle)
			self.focusQuantityInputTimer:cancel()
			self.focusQuantityInputTimer:start()
		end

		if self.numpad then
			self.numpad:show()
			self.numpad:setTitle(castingTitle)
		end
	else
		for _, v in ipairs(self.castingSelectors) do
			v:show()
		end

		if self.quantityInput then
			self.focusQuantityInputTimer:cancel()
			self.quantityInput:hide()
		end

		if self.numpad then
			self.numpad:hide()
		end
	end
end

function GuiHandler:confirmCasting(quantity)
	local fluid = self.selectedFluid
	local castingType = self.castingType

	self:selectCastingType(nil)

	if not fluid or not castingType or type(quantity) ~= "number" or quantity == 0 then
		return
	end

	local success, errMessage = self.controller:queueCasting(fluid, castingType, quantity)
	local fluidName = fluid.name
	local castingTypeText = castingType.displayName

	if quantity > 1 then
		castingTypeText = castingTypeText .. "s"
	end

	local castingJobText = ("%d %s %s"):format(quantity, fluidName, castingTypeText)

	if success then
		self:showToast("Queued casting of " .. castingJobText, colors.green)
	else
		errMessage = errMessage or "Unknown error"
		self:showToast(("Couldn't cast %s: %s"):format(castingJobText, errMessage), colors.red)
	end
end

function GuiHandler:onFluidSelectionChanged(_, _, item)
	if self.inFluidChangedHandler then
		return
	end

	pcall(function()
		self.inFluidChangedHandler = true

		local fluid = item and item.args[1]

		self:setSelectedFluid(fluid)
	end)

	self.inFluidChangedHandler = false
end

function GuiHandler:onSearchChanged(_, _, value)
	if self.inSearchChangedHandler then
		return
	end

	pcall(function()
		self.inSearchChangedHandler = true
		self:setSearchText(value)
	end)

	self.inSearchChangedHandler = false
end

function GuiHandler:onSearchClicked(_, _, button)
	if button == 2 then
		self:setSearchText("")
	end
end

function GuiHandler:onToastTimerCalled()
	for _, label in ipairs(self.toastLabels) do
		label:setText(""):hide()
	end
end

--#endregion GuiHandler

return GuiHandler
