local M = {}

function M.clamp(x, xMin, xMax)
	return math.min(math.max(x, xMin), xMax)
end

function M.round(number)
	return math.floor(number + 0.5)
end

function M.properCase(str)
	local result=''
    for word in string.gmatch(str, "%S+") do          
        local first = string.sub(word,1,1)
        result = (result .. string.upper(first) ..
            string.lower(string.sub(word,2)) .. ' ')
    end
    return result:sub(0, #result - 1)
end

return M