-- Imports
local diggerlib = require("diggerlib")

-- Module locals
local isFuel = diggerlib.isFuel

-- Program
local function printUsage()
	print("Usage: build <dx> <dz> [<sx> <sz>]")
end

local tArgs = { ... }

if #tArgs ~= 2 and #tArgs ~= 4 then
	printUsage()
	return
end

local function validateArg(val, name, allowNegative)
	local err

	err, val = pcall(function() return tonumber(val) end)

	if not err or type(val) ~= "number" then
		print("Error: " .. name .. " must be a number")
		printUsage()
		return false
	end

	if val < 0 and not allowNegative then
		print("Error: " .. name .. " must be positive")
		printUsage()
		return false
	end

	return true
end

if not validateArg(tArgs[1], "dx") or not validateArg(tArgs[2], "dz") then
	return
end

local dx = tonumber(tArgs[1])
local dz = tonumber(tArgs[2])
local sx = 0
local sz = 0

if #tArgs == 4 then
	if not validateArg(tArgs[3], "sx", true) or not validateArg(tArgs[4], "sz", true) then
		return
	end

	sx = tonumber(tArgs[3])
	sz = tonumber(tArgs[4])
end

-- Print wrapper
if fs.exists("build.log") then fs.delete("build.log") end
local logFile = fs.open("build.log", "w")
local _print = _G["print"]

_G["print"] = function(msg)
	logFile.write(msg)
	logFile.write("\n")
	_print(msg)
end

local digger = diggerlib.Digger()
local fuelNeeded = dx * dz + sx + sz

if not digger:refuel(fuelNeeded) then
	print("Not enough fuel. Have " .. turtle.getFuelLevel() .. " but need " .. fuelNeeded .. ".")
	return
end

print("Building...")
turtle.select(1)

if sx ~= 0 or sz ~= 0 then
	digger:goTo(sx, 0, sz, 0, 1)
end

local function findItem()
	if turtle.getItemCount() > 0 and not isFuel(turtle.getItemDetail(nil, true)) then
		return true
	end

	for slot = 1, 16 do
		if turtle.getItemCount(slot) > 0 and not isFuel(turtle.getItemDetail(slot, true)) then
			turtle.select(slot)
			return true
		end
	end

	return false
end

local function tryBuild()
	while not findItem() do
		print("No items to place! Please put something in my inventory.")
		os.sleep(1)
	end

	if not turtle.compareDown() then
		turtle.digDown()
	end

	turtle.placeDown()

	return digger:tryMove("forward")
end

local function runBuilder()
	local evenWidth = math.fmod(dx, 2) == 0

	for x = 1, dx do
		for _ = 1, dz - 1 do
			if not tryBuild() then
				return
			end
		end

		if x < dx then
			-- Turn 90, move in x axis, then turn 90 again

			local turnLeft

			if evenWidth then
				turnLeft = math.fmod(x, 2) ~= 0
			else
				turnLeft = math.fmod(x, 2) == 0
			end

			if turnLeft then
				digger:turnLeft()

				if not tryBuild() then
					return
				end

				digger:turnLeft()
			else
				digger:turnRight()

				if not tryBuild() then
					return
				end

				digger:turnRight()
			end
		else
			-- Turn 180

			digger:turnLeft()
			digger:turnLeft()
		end
	end
end

runBuilder()
print( "Returning to surface..." )

-- Return to where we started digging
digger:goTo(sx, 0, sz, nil, nil)
-- Now return to where the program was started in the opposite move order
digger:goTo(0, 0, 0, 0, -1, "yxz")
-- Turn to face the direction we started
digger:goTo(0, 0, 0, 0, 1)

print("Done!")