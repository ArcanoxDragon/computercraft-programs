local M = {}

local pMax, iMax, dMax = 0, 0, 0
local BAR_WIDTH = 33

local function clamp(value, _min, _max)
	return math.min(math.max(value, _min), _max)
end

local function setTextColor(color)
	if term.isColor() then term.setTextColor(color) end
end

local function setBackgroundColor(color)
	if term.isColor() then term.setBackgroundColor(color) end
end

local function printBar(x, y, w, value, valueMax, color)
	local c = color or colors.green
	if term.isColor() then
		term.setCursorPos(x, y)
		setBackgroundColor(colors.gray)
		term.write(string.rep(" ", w))
	end
	term.setCursorPos(x + (w / 2), y)
	setBackgroundColor(c)
	term.write(" ")
	local pct = clamp(math.abs(value) / valueMax, 0.0, 1.0)
	if value > 0 then
		term.setCursorPos(x + (w / 2), y)
		term.write(string.rep(" ", pct * (w / 2)))
	elseif value < 0 then
		term.setCursorPos(x + (w / 2) - pct * (w / 2) + 1, y)
		term.write(string.rep(" ", pct * (w / 2)))
	end
end

function M.printPid(p, i, d)
	if math.abs(p) > pMax then pMax = math.abs(p) end
	if math.abs(i) > iMax then iMax = math.abs(i) end
	if math.abs(d) > dMax then dMax = math.abs(d) end
	
	setBackgroundColor(colors.black)
	setTextColor(colors.white)
	term.clear()
	term.setCursorPos(2, 2)
	term.write("P:")
	term.setCursorPos(2, 4)
	term.write("I:")
	term.setCursorPos(2, 6)
	term.write("D:")
	setTextColor(colors.red)
	term.setCursorPos(5, 2)
	term.write(p)
	setTextColor(colors.green)
	term.setCursorPos(5, 4)
	term.write(i)
	setTextColor(colors.blue)
	term.setCursorPos(5, 6)
	term.write(d)
	printBar(2, 3, BAR_WIDTH, p, pMax, colors.red)
	printBar(2, 5, BAR_WIDTH, i, iMax, colors.green)
	printBar(2, 7, BAR_WIDTH, d, dMax, colors.blue)
end

return M