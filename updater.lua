local filename
local tArgs = { ... }

if #tArgs >= 1 then
	filename = tArgs[1]
else
	filename = "updater.cfg"
end

print("Using config: " .. filename)

if not http then
	print("This program requires the HTTP API to be enabled for ComputerCraft")
	print("Please enable the API in the ComputerCraft configuration, restart Minecraft, and run the program again")
	return
end

if fs.exists("updater.log") then fs.delete("updater.log") end
local logFile = fs.open("updater.log", "w")

local function logPrint(msg)
	logFile.write(msg)
	logFile.write("\n")
	print(msg)
end

local function logWrite(msg)
	logFile.write(msg)
	write(msg)
end

local programs = { }
local defConfig = { programName = "http://example.com/url-to-plaintext-program.lua", otherProgramName = "http://example.com/other-url-to-plaintext-program.lua" }

if not fs.exists(filename) then
	logPrint("Downloading default config...please re-run this program after it finishes")
	local handle = http.get("https://bitbucket.org/ArcanoxDragon/computercraft-programs/raw/master/updater.cfg")
	local text = handle.readAll()
	handle.close()
	local file = fs.open(filename, "w")
	file.write(text)
	file.close()
	return
end

local success, config = pcall(function() return require("config") end)

if success then
	local cfg = config.open(filename, defConfig)
	
	for k, v in cfg:values() do
		programs[k] = v
	end
else
	if not fs.exists(filename) then
		logPrint("Downloading default config...please re-run this program after it finishes")
		local handle = http.get("https://bitbucket.org/ArcanoxDragon/computercraft-programs/raw/master/updater.cfg")
		local text = handle.readAll()
		handle.close()
		local file = fs.open(filename, "w")
		file.write(text)
		file.close()
	end
	
	local file = fs.open(filename, "r")
	local txt = file.readAll()
	file.close()

	for k, v in string.gmatch(txt, "([^=\n]+)=([^=\n]+)") do
		programs[k] = v
	end
end

for k, v in pairs(programs) do
	local programName = k
	local programUrl = v
	
	logWrite("Updating \"" .. programName .. "\"...")
	logWrite("(Downloading from " .. programUrl .. ")")
	
	local response, err = http.get(programUrl)
	
	if response then
		local programText = response.readAll()
		response.close()
		
		if fs.exists(programName) then fs.delete(programName) end
		if fs.exists(programName) then
			logPrint("Error:")
			logPrint("Could not delete \"" .. programName .. "\"...make sure the file is writable by Minecraft")
			logPrint("")
		else
			local fileName = programName
			
			if not string.match(fileName, ".+%..+$") then -- Add a .lua extension if no extension was provided
				fileName = fileName .. ".lua"
			end
			
			local newFile = fs.open(fileName, "w")
			newFile.write(programText)
			newFile.close()
			logPrint("Done.")
		end
	else
		logPrint("Error:")
		logPrint("Could not download program \"" .. programName .. "\" (" .. err .. ")")
		logPrint("")
	end
end

logFile.close()