package.path = package.path .. ";../?.lua"

-- Imports
local basalt = require("basalt")
local configLib = require("config")
local ReactorController = require("ReactorController")
local GuiController = require("GuiController")

-- Constants
local DEFAULT_CONFIG = {
	coolant_kP = 4,
	coolant_kI = 5,
	coolant_kD = 0.05,
	energy_kP = 2,
	energy_kI = 5,
	energy_kD = 0.01,
	temperatureHardLimit = 700,
	maximumEnergyRatio = 0.95,
	minimumCoolantRatio = 0.2,
	maximumWasteRatio = 0.05,
	emergencyRedstoneSignalPeripheral = "",
	emergencyRedstoneSignalSide = "",
}

-- Local vars
local config = configLib.open("/reactor.cfg", DEFAULT_CONFIG)
local reactor, turbine
local controller, gui

-- Initialize
if type(config.reactorName) == "string" then
	reactor = peripheral.wrap(config.reactorName)
else
	reactor = peripheral.find("fissionReactorLogicAdapter")
end

if type(config.turbineName) == "string" then
	turbine = peripheral.wrap(config.turbineName)
else
	turbine = peripheral.find("turbineValve")
end

if not reactor then
	error("Reactor logic adapter not found!")
end
if not turbine then
	error("Turbine valve not found!")
end

-- SCRAM reactor while initializing
if reactor.getStatus() then
	reactor.scram()
end

controller = ReactorController.new(reactor, turbine, config)
gui = GuiController.new(controller)

-- Create separate threads to handle updating logic
local function updateController()
	local reportedError = false

	while true do
		controller:update(0.05)
		os.sleep(0.05)

		if controller.lastError then
			if not reportedError then
				reportedError = true
				basalt.log("Error in controller: " .. controller.lastError)
			end
		else
			reportedError = false
		end
	end
end

local function updateGui()
	while true do
		local success, err = pcall(gui.update, gui)

		if success then
			os.sleep(0.25)
		else
			basalt.log("Error in GUI: " .. tostring(err))
			os.sleep(1)
		end
	end
end

gui.frame:addThread():start(updateController)
gui.frame:addThread():start(updateGui)

-- Start Basalt event loop
basalt.autoUpdate()
