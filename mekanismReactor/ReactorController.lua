-- Imports
local expect = require("cc.expect").expect
local PID = require("pid").PID
local util = require("arc_util")

-- Local aliases
local mapRange = util.mapRange

-- Static init
settings.define("reactor.desired_state", {
	description = "The desired state of the reactor",
	default = false,
	type = "boolean",
})

-- Class
local ReactorController = {}
ReactorController.__index = ReactorController

function ReactorController.new(reactor, turbine, config)
	expect(1, reactor, "table")
	expect(2, turbine, "table")
	expect(3, config, "table")

	local object = {
		reactor = reactor,
		turbine = turbine,
		config = config,

		maxBurnRate = type(config.maximumBurnRate) == "number" and config.maximumBurnRate or reactor.getMaxBurnRate(),
		tempHardLimit = type(config.temperatureHardLimit) == "number" and config.temperatureHardLimit or 500,
		maxEnergyRatio = type(config.maximumEnergyRatio) == "number" and config.maximumEnergyRatio or 0.95,
		minCoolantRatio = type(config.minimumCoolantRatio) == "number" and config.minimumCoolantRatio or 0.2,
		maxWasteRatio = type(config.maximumWasteRatio) == "number" and config.maximumWasteRatio or 0.05,

		desiredState = settings.get("reactor.desired_state", false),
		didScram = false,
		doReset = false,
		scramReason = "",
		lastError = nil,

		pidCoolant = PID({
			kP = type(config.coolant_kP) == "number" and config.coolant_kP or 4,
			kI = type(config.coolant_kI) == "number" and config.coolant_kI or 5,
			kD = type(config.coolant_kD) == "number" and config.coolant_kD or 0.05,
			minOutput = 0,
			maxOutput = 1,
		}),

		pidEnergy = PID({
			kP = type(config.energy_kP) == "number" and config.energy_kP or 2,
			kI = type(config.energy_kI) == "number" and config.energy_kI or 5,
			kD = type(config.energy_kD) == "number" and config.energy_kD or 0.01,
			minOutput = 0,
			maxOutput = 1,
		}),
	}
	setmetatable(object, ReactorController)

	object:resetEmergency()

	return object
end

function ReactorController:update(dt)
	local success, err = pcall(self.unsafeUpdate, self, dt)

	if not success then
		self:scram("Fatal Error")
		self.lastError = err
	end
end

function ReactorController:unsafeUpdate(dt)
	if self.doReset then
		self:resetEmergency()
	end

	if self.didScram then
		return
	end

	local safety, scramReason = self:safetyCheck()

	if not safety then
		self:scram(scramReason)
		return
	end

	if self.desiredState then
		if not self.reactor.getStatus() then
			self.reactor.activate()
		end

		if self.turbine.getDumpingMode() ~= "IDLE" then
			self.turbine.setDumpingMode("IDLE")
		end

		local coolantRatio = self.reactor.getCoolantFilledPercentage()
		local energyRatio = self.turbine.getEnergyFilledPercentage()
		local targetBurnRate

		if energyRatio >= self.maxEnergyRatio then
			targetBurnRate = 0
			self.pidCoolant:reset()
			self.pidEnergy:reset()
		else
			-- Coolant ratio is mapped to a range of -1 (Full) to 1 (Empty).
			-- The PID target is set to 0, meaning at full, the PID will trend upwards to 1 (max output).
			-- At empty, the PID will trend downwards to 0 (no output).
			local coolantFactor = mapRange(coolantRatio, 0, 1, 1, -1)

			-- Energy ratio is mapped to a range of -1 (Empty) to 1 (Full).
			-- The PID target is set to 0, meaning at full, the PID will trend downwards to 0 (no output).
			-- At empty, the PID will trend upwards to 1 (max output).
			local energyFactor = mapRange(energyRatio, 0, 1, -1, 1)

			local coolantControl = self.pidCoolant:get(0, coolantFactor, dt)
			local energyControl = self.pidEnergy:get(0, energyFactor, dt)
			local finalControl = coolantControl * energyControl

			targetBurnRate = finalControl * self.maxBurnRate
		end

		self.reactor.setBurnRate(targetBurnRate)
	else
		if self.reactor.getStatus() then
			self.reactor.scram()
		end
	end
end

function ReactorController:safetyCheck()
	local r = self.reactor
	local temp = r.getTemperature()
	local coolantRatio = r.getCoolantFilledPercentage()
	local wasteRatio = r.getWasteFilledPercentage()

	if temp > self.tempHardLimit then
		return false, ("Reactor temperature of %d was greater than hard limit of %d"):format(temp, self.tempHardLimit)
	end

	if coolantRatio < self.minCoolantRatio then
		return false, ("Coolant ratio of %.2f was less than the minimum required of %.2f")
			:format(coolantRatio, self.minCoolantRatio)
	end

	if wasteRatio > self.maxWasteRatio then
		return false, ("Waste ratio of %.2f was greater than the maximum allowed of %.2f")
			:format(wasteRatio, self.maxWasteRatio)
	end

	return true, nil
end

function ReactorController:getState()
	return self.desiredState
end

function ReactorController:setState(state)
	self.desiredState = state or false
	settings.set("reactor.desired_state", self.desiredState)
	settings.save()
end

function ReactorController:scram(reason)
	self.didScram = true
	self.doReset = false
	self.scramReason = reason or "Unknown Reason"
	if self.reactor.getStatus() then
		self.reactor.scram()
	end
	self.reactor.setBurnRate(0)
	self.turbine.setDumpingMode("DUMPING_EXCESS")
	self:setEmergencySignal(true)
	self:setState(false)
end

function ReactorController:requestResetEmergency()
	self.doReset = true
end

function ReactorController:resetEmergency()
	self.doReset = false
	self.didScram = false
	self.scramReason = ""
	self.turbine.setDumpingMode("IDLE")
	self:setEmergencySignal(false)
end

function ReactorController:setEmergencySignal(signal)
	local emergencyRedstoneSignalPeripheral = self.config.emergencyRedstoneSignalPeripheral
	local emergencyRedstoneSignalSide = self.config.emergencyRedstoneSignalSide

	if type(emergencyRedstoneSignalPeripheral) ~= "string"
		or type(emergencyRedstoneSignalSide) ~= "string"
		or emergencyRedstoneSignalPeripheral:len() == 0
		or emergencyRedstoneSignalSide:len() == 0
	then
		return
	end

	peripheral.call(emergencyRedstoneSignalPeripheral, "setOutput", emergencyRedstoneSignalSide, signal)
end

return ReactorController
