-- Imports
local basalt = require("basalt")
local darkTheme = require("basalt_dark_theme")
local expect = require("cc.expect").expect

-- Class
local GuiController = {}
GuiController.__index = GuiController

function GuiController.new(reactorController)
	expect(1, reactorController, "table")

	local object = {
		reactor = reactorController,
		frame = nil,
	}
	setmetatable(object, GuiController)

	object:initialize()

	return object
end

function GuiController:initialize()
	basalt.setTheme(darkTheme.theme)

	local baseFrame = basalt.createFrame()

	darkTheme.installPalette(baseFrame)

	local frame = baseFrame:addFrame()
		:setSize("{parent.w - 2}", "{parent.h - 2}")
		:setPosition(2, 2)

	frame:addLabel()
		:setPosition(2, 2)
		:setText("Status:")

	frame:addLabel("statusLabel")
		:setPosition(16, 2)
		:setText("Deactivated")
		:setForeground(colors.red)

	frame:addLabel()
		:setPosition(2, 3)
		:setText("SCRAM reason:")

	frame:addLabel("scramReasonLabel")
		:setPosition(16, 3)
		:setSize("{parent.w - 17}", 2)
		:setText("None")

	frame:addButton("toggleStateButton")
		:setPosition(2, "{parent.h - self.h}")
		:setText("Activate")
		:setBackground(colors.green)
		:onClick(function()
			self:toggleState()
		end)

	frame:addButton("clearEmergencyButton")
		:setSize(19, 3)
		:setPosition("{toggleStateButton.x + toggleStateButton.w + 1}", "{parent.h - self.h}")
		:setText("Clear Emergency")
		:setBackground(colors.lightGray)
		:onClick(function()
			self:clearEmergency()
		end)

	self.frame = baseFrame

	frame:addLabel():setPosition(2, 5):setText("Coolant:")
	self:addPidDebug(frame, 2, 6, self.reactor.pidCoolant)
	frame:addLabel():setPosition(2, 13):setText("Energy:")
	self:addPidDebug(frame, 2, 14, self.reactor.pidEnergy)
end

function GuiController:addPidDebug(parent, x, y, pid)
	local frame = parent:addFrame()
		:setPosition(x, y)
		:setSize(20, 6)

	local tLabel = frame:addLabel()
		:setPosition(1, 1)
		:setText("T: 0")

	local aLabel = frame:addLabel()
		:setPosition(1, 2)
		:setText("A: 0")

	local pLabel = frame:addLabel()
		:setPosition(1, 3)
		:setText("P: 0")

	local iLabel = frame:addLabel()
		:setPosition(1, 4)
		:setText("I: 0")

	local dLabel = frame:addLabel()
		:setPosition(1, 5)
		:setText("D: 0")

	local oLabel = frame:addLabel()
		:setPosition(1, 6)
		:setText("O: 0")

	self.frame:addThread():start(function()
		while true do
			tLabel:setText(("T: %.4f"):format(pid.target))
			aLabel:setText(("A: %.4f"):format(pid.actual))
			pLabel:setText(("P: %.4f"):format(pid.pFactor))
			iLabel:setText(("I: %.4f"):format(pid.iFactor))
			dLabel:setText(("D: %.4f"):format(pid.dFactor))
			oLabel:setText(("O: %.4f"):format(pid.output))
			os.sleep(0.05)
		end
	end)
end

function GuiController:update()
	local statusLabel = self.frame:getDeepChildren("statusLabel")
	local scramReasonLabel = self.frame:getDeepChildren("scramReasonLabel")
	local toggleStateButton = self.frame:getDeepChildren("toggleStateButton")
	local clearEmergencyButton = self.frame:getDeepChildren("clearEmergencyButton")

	if self.reactor.didScram then
		statusLabel:setText("SCRAM"):setForeground(colors.red)
		scramReasonLabel:setText(self.reactor.scramReason or "Unknown Reason"):setForeground(colors.white)
		toggleStateButton:setText("Locked Out"):setBackground(colors.lightGray):disable()
		clearEmergencyButton:setBackground(basalt.getTheme("ButtonBG"))
	else
		scramReasonLabel:setText("None"):setForeground(statusLabel:getParent():getForeground())
		toggleStateButton:enable()
		clearEmergencyButton:setBackground(colors.lightGray)

		if self.reactor:getState() then
			statusLabel:setText("Active"):setForeground(colors.green)
			toggleStateButton:setText("Deactivate"):setBackground(colors.red)
		else
			statusLabel:setText("Deactivated"):setForeground(colors.red)
			toggleStateButton:setText("Activate"):setBackground(colors.green)
		end
	end
end

function GuiController:toggleState()
	if self.reactor.didScram then
		return
	end

	self.reactor:setState(not self.reactor:getState())
end

function GuiController:clearEmergency()
	self.reactor:requestResetEmergency()
end

return GuiController
