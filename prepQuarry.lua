local USE_END_MARKER = true -- Change this if you want to use fences

local args = { ... }

if #args ~= 2 then
	print("Usage: prepQuarry <width> <depth>")
	print("    width: the inner (working) width of the quarry area")
	print("    depth: the inner (working) depth of the quarry area")
	return
end

local width = tonumber(args[1])
local depth = tonumber(args[2])

if not width or not depth then
	print("Width and depth must be numbers")
	return
end

width = width + 1 -- We're working with inner size, not outer
depth = depth + 1

local totalTravelDistance = (width * 2) + (depth * 2) - 1

if USE_END_MARKER then
	local endMarkers = 0
	
	for i = 1, 16 do
		local item = turtle.getItemDetail(i)
		if item and item.name == "ExtraUtilities:endMarker" then
			endMarkers = endMarkers + item.count
		end
	end
	
	turtle.select(1)

	if endMarkers < 4 then
		print("Not enough Ender Markers!")
		print("Needed: 4")
		print("Have:   " .. tostring(endMarkers))
		print("Need:   " .. tostring(4 - endMarkers) .. " more")
		return
	end
else
	local curFences = 0

	for i = 1, 16 do
		local item = turtle.getItemDetail(i)
		if item and item.name == "minecraft:fence" then
			curFences = curFences + item.count
		end
	end

	turtle.select(1)

	if curFences < totalTravelDistance then
		print("Not enough fences!")
		print("Needed: " .. tostring(totalTravelDistance))
		print("Have:   " .. tostring(curFences))
		print("Need:   " .. tostring(totalTravelDistance - curFences) .. " more")
		return
	end
end

local fuel = turtle.getFuelLevel()
if fuel ~= "unlimited" then
	if fuel < totalTravelDistance + 4 then
		print("Not enough fuel! Please insert coal, charcoal, or planks into the bottom-right slot.")
		turtle.select(16)
		while turtle.getFuelLevel() < totalTravelDistance do
			local item = turtle.getItemDetail(16)
			if item ~= nil then
				if item.name == "minecraft:coal" or item.name == "minecraft:planks" then
					turtle.refuel(item.count)
				end
			end
			os.sleep(1)
		end
		print("Thank you!")
		turtle.select(1)
	end
end

if fs.exists("quarry") then
	print("A quarry file already exists on this turtle. If the quarry is still there, please run destroyQuarry.")
	write("Would you like to force-delete the file and build a new quarry? (y/n) > ")
	local input
	local good = false
	while not good do
		input = io.read():lower()
		if input == "y" or input == "n" then
			good = true
		else
			write("Please enter \"y\" or \"n\". > ")
		end
	end
	if input == "y" then
		fs.delete("quarry")
	else
		return
	end
end
local qFile = fs.open("quarry", "w")
qFile.write(tostring(width - 1) .. "\n" .. tostring(depth - 1) .. "\n" .. (USE_END_MARKER and "1" or "0"))
qFile.close()

print("Building quarry!")

local function fwd()
	while not turtle.forward() do
		turtle.dig()
		turtle.attack()
	end
end

local function place()
	local selItem = turtle.getItemDetail(turtle.getSelectedSlot())
	local neededItem = (USE_END_MARKER and "ExtraUtilities:endMarker" or "minecraft:fence")
	if selItem == nil or selItem.count < 1 or selItem.name ~= neededItem then
		local found = false
		for i = 1, 16 do
			selItem = turtle.getItemDetail(i)
			if selItem and selItem.count > 0 and selItem.name == neededItem then
				turtle.select(i)
				found = true
				break
			end
		end
		if not found then
			print("Not enough " .. (USE_END_MARKER and "Ender Markers" or "fences") .. "!")
			print("Needed: " .. tostring((USE_END_MARKER and 4 or totalTravelDistance)))
			print("Have:   " .. 0)
			print("Need:   " .. tostring((USE_END_MARKER and 4 or totalTravelDistance)) .. " more")
			print("Please insert more " .. (USE_END_MARKER and "Ender Markers" or "fences") .. ".")
			local curItems = 0
			while curItems < (USE_END_MARKER and 4 or totalTravelDistance) do
				for i = 1, 16 do
					local item = turtle.getItemDetail(i)
					if item ~= nil and item.name == neededItem then
						curItems = curItems + turtle.getItemCount(i)
					end
				end
				os.sleep(2)
			end
			
			print("Thank you!")
		end
	end
	while turtle.detectDown() do
		turtle.digDown()
		turtle.attackDown()
	end
	turtle.placeDown()
	totalTravelDistance = totalTravelDistance - 1
end

if USE_END_MARKER then
	fwd()
	place()
end

for i = 1, width do
	fwd()
	if not USE_END_MARKER then place() end
end

turtle.turnRight()
if USE_END_MARKER then place() end

for i = 1, depth do
	fwd()
	if not USE_END_MARKER then place() end
end

turtle.turnRight()
if USE_END_MARKER then place() end

for i = 1, width do
	fwd()
	if not USE_END_MARKER then place() end
end

turtle.turnRight()
if USE_END_MARKER then place() end

for i = 1, width - 1 do
	fwd()
	if not USE_END_MARKER then place() end
end

fwd()
turtle.turnRight()
if USE_END_MARKER then
	turtle.back()
end
if turtle.detectUp() then
	turtle.digUp()
end
turtle.up()
print("Quarry complete!")