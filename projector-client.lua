local tArgs = { ... }
local modem = nil
local dual = { }
local toChannel = (#tArgs >= 1 and tonumber(tArgs[1])) or 9
local endProgram = (#tArgs >= 2 and tArgs[2] or "shell")
local fromChannel = os.getComputerID()

local curTerm = term.current()

local function cLog(msg)
	local file = fs.open("redir_log", "a")
	file.write(msg .. "\n")
	file.close()
end

cLog("Connecting to projector...")

for _,v in pairs(rs.getSides()) do
	if peripheral.getType(v) == "modem" then
		modem = peripheral.wrap(v)
		break
	end
end

if not modem then
	print("No modem attached")
	return
end

local function split(text, delimiter)
	local resTable, lastLocation
	resTable = {}
	lastLocation = 0
	if #text == 1 then return {text} end
	while true do
		local l = string.find(text, delimiter, lastLocation, true)
		if l ~= nil then
			table.insert(resTable, string.sub(text, lastLocation, l - 1))
			lastLocation = l + 1
		else
			table.insert(resTable, string.sub(text, lastLocation))
			break
		end
	end
	return resTable
end

print("Connecting to projector...")
modem.open(fromChannel)
modem.transmit(toChannel, fromChannel, "HI!")
local isColor, pWidth, pHeight
while true do
	os.startTimer(5) -- 5 second timeout
	
	local e, _, _, rChannel, msg, _ = os.pullEvent()
	
	if e == "timer" then
		print("Cannot connect to projector.")
		error() -- exit
	elseif e == "modem_message" then
		if rChannel ~= fromChannel then
			if msg:find("WELCOME") then
				local mArgs = split(msg, ":")
				if #mArgs ~= 4 then
					print("Target device is not a projector")
					error()
				end
				isColor = tonumber(mArgs[2])
				if isColor < 0 then isColor = 0 end
				if isColor > 1 then isColor = 0 end
				pWidth = tonumber(mArgs[3])
				pHeight = tonumber(mArgs[4])
				break
			else
				print("Target device is not a projector")
				error()
			end
		end
	end
end

print("Connected. Mirroring displays.")

local function send(msg)
	modem.transmit(toChannel, fromChannel, msg)
end

local project = {}
project["write"] = function(text)
	send("WRITE:" .. text)
end
project["clear"] = function()
	send("CLEAR")
end
project["clearLine"] = function()
	send("CLEARLINE")
end
project["getCursorPos"] = function() return 1, 1 end
project["setCursorPos"] = function(x, y)
	send("SETCUR:" .. tostring(x) .. ":" .. tostring(y))
end
project["setCursorBlink"] = function(blink)
	send("BLINK:" .. (blink and "1" or "0"))
end
project["isColor"] = function()
	return isColor > 0
end
project["isColour"] = project["isColor"] --this shouldn't be here. stupid england.
project["getSize"] = function()
	return pWidth, pHeight
end
project["scroll"] = function(lines)
	send("SCROLL:" .. tostring(lines))
end
project["redirect"] = function(target) return end
project["setTextColor"] = function(color)
	send("TCOLOR:" .. tostring(color))
end
project["setTextColour"] = project["setTextColor"] --this shouldn't be here. stupid england.
project["setBackgroundColor"] = function(color)
	send("BCOLOR:" .. tostring(color))
end
project["setBackgroundColour"] = project["setBackgroundColor"] --this shouldn't be here. stupid england.

local function bumpLine()
	local x, y = curTerm.getCursorPos()
	local w, h = curTerm.getSize()
	if y + 1 <= h then
		curTerm.setCursorPos(1, y + 1)
	else
		curTerm.setCursorPos(1, y)
		curTerm.scroll(1)
	end
end

for k,v in pairs(term) do
	dual[k] = function(...)
		project[k](...)
		return curTerm[k](...)
	end
end
 
dual.setCursorPos = function(x,y)
	curTerm.setCursorPos(x,y)
	local mw,mh = project.getSize()
	local tw,th = curTerm.getSize()
	local dw,dh = mw-tw,mh-th
	if dh > 0 then y = y + 1 end
	project.setCursorPos(x,y)
end

dual.clear = function()
	curTerm.clear()
	project.clear()
	local mw,mh = project.getSize()
	local tw,th = curTerm.getSize()
	local ldw,ldh = mw-tw,mh-th

	project.setBackgroundColor(colors.green)
	if ldh > 0 then
		project.setCursorPos(1,1)
		project.setTextColor(colors.white)
		local msg = "Projection"
		project.write(string.rep(" ", math.floor(mw/2-#msg/2))..
		msg..string.rep(" ", math.ceil(mw/2-#msg/2)))
		for i=ldw-2,0,-1 do
			project.setCursorPos(1,mh-i+1)
			project.write(string.rep(" ", mw))
		end
	end
	project.setBackgroundColor(colors.black)
end

print("Redirecting to monitor...")
sleep(2)
term.redirect(dual)
term.clear()
term.setCursorPos(1, 1)
if #tArgs <= 2 then
	shell.run(endProgram)
else
	local nArgs = { }
	for i = 3, #tArgs do
		nArgs[#nArgs + 1] = tArgs[i]
	end
	shell.run(endProgram, unpack(nArgs))
end