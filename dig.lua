-- Imports
local diggerlib = require("diggerlib")

-- Program
local function printUsage()
	print("Usage: dig <dx> <dy> <dz> [<sx> <sy> <sz>]")
end

local tArgs = { ... }

if #tArgs ~= 3 and #tArgs ~= 6 then
	printUsage()
	return
end

local function validateArg(val, name, allowNegative)
	local err
	
	err, val = pcall(function() return tonumber(val) end)
	
	if not err or type(val) ~= "number" then
		print("Error: " .. name .. " must be a number")
		printUsage()
		return false
	end
	
	if val < 0 and not allowNegative then
		print("Error: " .. name .. " must be positive")
		printUsage()
		return false
	end
	
	return true
end

if not validateArg(tArgs[1], "dx") or not validateArg(tArgs[2], "dy") or not validateArg(tArgs[3], "dz") then
	return
end

local dx = tonumber(tArgs[1])
local dy = tonumber(tArgs[2])
local dz = tonumber(tArgs[3])
local sx = 0
local sy = 0
local sz = 0

if #tArgs == 6 then
	if not validateArg(tArgs[4], "dx", true) or not validateArg(tArgs[5], "dy", true) or not validateArg(tArgs[6], "dz", true) then
		return
	end
	
	sx = tonumber(tArgs[4])
	sy = tonumber(tArgs[5])
	sz = tonumber(tArgs[6])
end

-- Print wrapper
if fs.exists("dig.log") then fs.delete("dig.log") end
local logFile = fs.open("dig.log", "w")
local _print = _G["print"]

_G["print"] = function(msg)
	logFile.write(msg)
	logFile.write("\n")
	_print(msg)
end

local digger = diggerlib.Digger()
local fuelNeeded = dx * dy * dz + sx + sy + sz

if not digger:refuel(fuelNeeded) then
	print("Not enough fuel. Have " .. turtle.getFuelLevel() .. " but need " .. fuelNeeded .. ".")
	return
end

print("Digging...")
turtle.select(1)

if sx ~= 0 or sy ~= 0 or sz ~= 0 then
	digger:goTo(sx, sy, sz, 0, 1)
end

local function runDigger()
	local evenWidth = math.fmod(dx, 2) == 0
	
	for y = 1, dy do
		for x = 1, dx do
			for z = 1, dz - 1 do
				if not digger:tryMove("forward") then
					return
				end
			end
			
			if x < dx then
				-- Turn 90, move in x axis, then turn 90 again
				
				local turnLeft
				
				if evenWidth then
					turnLeft = math.fmod(x, 2) ~= math.fmod(y, 2)
				else
					turnLeft = math.fmod(x, 2) == 0
				end
				
				if turnLeft then
					digger:turnLeft()
					
					if not digger:tryMove("forward") then
						return
					end
					
					digger:turnLeft()
				else
					digger:turnRight()
					
					if not digger:tryMove("forward") then
						return
					end
					
					digger:turnRight()
				end
			else
				-- Turn 180
				
				digger:turnLeft()
				digger:turnLeft()
			end
		end
		
		if y < dy then
			-- Move up
			
			digger:tryMove("up")
		end
	end
end

runDigger()
print( "Returning to surface..." )

-- Return to where we started digging
digger:goTo(sx, sy, sz, nil, nil)
-- Now return to where the program was started in the opposite move order
digger:goTo(0, 0, 0, 0, -1, "yxz")
-- Check to see if there's a chest behind where we started
if diggerlib.isFacingInventory() then
	-- Unload into that chest
	diggerlib.unloadItems()
end
-- Turn to face the direction we started
digger:goTo(0, 0, 0, 0, 1)

print("Mined " .. digger.totalCollected .. " items total.")