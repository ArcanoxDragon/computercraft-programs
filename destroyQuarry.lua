if not fs.exists("quarry") then
	print("No quarry file found on computer! You will need to destroy the quarry manually.")
	return
end

local qFile = fs.open("quarry", "r")
local line1 = qFile.readLine()
local line2 = qFile.readLine()
local line3 = qFile.readLine()
qFile.close()

if line1 == nil or line2 == nil or line3 == nil then
	print("Quarry file is corrupt! You will need to destroy the quarry manually.")
	fs.delete("quarry")
	return
end

local w = tonumber(line1)
local h = tonumber(line2)
local endMarker = tonumber(line3) and (tonumber(line3) > 0)

if w == nil or h == nil or endMarker == nil then
	print("Quarry file is corrupt! You will need to destroy the quarry manually.")
	fs.delete("quarry")
	return
end

w = w + 1
h = h + 1
local fuelNeeded = (w * 2) + (h * 2) - 1
local fuel = turtle.getFuelLevel()
if fuel ~= "unlimited" then
	if fuel < fuelNeeded + 4 then
		print("Not enough fuel! Please insert coal, charcoal, or planks into the bottom-right slot.")
		turtle.select(16)
		while turtle.getFuelLevel() < fuelNeeded do
			local item = turtle.getItemDetail(16)
			if item ~= nil then
				if item.name == "minecraft:coal" or item.name == "minecraft:planks" then
					turtle.refuel(item.count)
				end
			end
			os.sleep(1)
		end
		print("Thank you!")
	end
end

print("Destroying quarry...")
fs.delete("quarry")

turtle.down()

local function fwd()
	while not turtle.forward() do
		turtle.dig()
		turtle.attack()
	end
end

local function destroy()
	while turtle.detectDown() do
		turtle.digDown()
		turtle.attackDown()
	end
end

if endMarker then
	fwd()
	destroy()
end -- End marker boundaries are shifted 1 forward

for i = 1, w do
	fwd()
	if not endMarker then destroy() end
end

turtle.turnRight()
if endMarker then destroy() end

for i = 1, h do
	fwd()
	if not endMarker then destroy() end
end

turtle.turnRight()
if endMarker then destroy() end

for i = 1, w do
	fwd()
	if not endMarker then destroy() end
end

turtle.turnRight()
if endMarker then destroy() end

for i = 1, h - 1 do
	fwd()
	if not endMarker then destroy() end
end

turtle.forward()
turtle.turnRight()
if endMarker then
	destroy()
	turtle.back()
end
print("Quarry destroyed!")