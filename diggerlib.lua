local M = {}

-- Constants
local VALID_FUEL = {
	"minecraft:coals",
	"forge:rods/blaze"
}
local VALID_DIRECTIONS = {
	forward = true,
	back = true,
	up = true,
	down = true,
}
local VALID_ACTIONS = {
	dig = true,
	attack = true,
	detect = true,
}
local KEYS = {
	SPACE = 57,
}

-- Functions
local function sign(num)
	return num > 0 and 1 or (num < 0 and -1 or 0)
end

local function isFuel(item)
	for i = 1, #VALID_FUEL do
		if item.tags[VALID_FUEL[i]] then
			return true
		end
	end

	return false
end

local function validateDirection(direction)
	if not VALID_DIRECTIONS[direction] then
		error("Invalid direction: " .. direction)
	end
end

local function validateAction(action)
	if not VALID_ACTIONS[action] then
		error("Invalid action: " .. action)
	end
end

local function validateMoveOrder(order)
	if type(order) ~= "string" then error("Move order must be a string") end
	if string.len(order) ~= 3 then error("Move order must have exactly 3 characters") end

	local x = 0
	local y = 0
	local z = 0

	for i = 1, 3 do
		local char = string.sub(order, i, i)

		if char == "x" then
			if x ~= 0 then error("Duplicate move instruction: " .. char) end
			x = i
		elseif char == "y" then
			if y ~= 0 then error("Duplicate move instruction: " .. char) end
			y = i
		elseif char == "z" then
			if z ~= 0 then error("Duplicate move instruction: " .. char) end
			z = i
		else
			error("Invalid move instruction: " .. char)
		end
	end

	if x == 0 then error("Missing move instruction: x") end
	if y == 0 then error("Missing move instruction: y") end
	if z == 0 then error("Missing move instruction: z") end

	local moves = {
		[x] = "x",
		[y] = "y",
		[z] = "z",
	}

	return moves
end

local function hasRoom()
	local doesHaveRoom = false

	for slot = 1, 16 do
		local inSlot = turtle.getItemCount(slot)

		if inSlot == 0 then
			doesHaveRoom = true
		end
	end

	return doesHaveRoom
end

local function getTotalItemCount()
	local itemCount = 0

	for slot = 1, 16 do
		local inSlot = turtle.getItemCount(slot)

		itemCount = itemCount + inSlot
	end

	return itemCount
end

local function isFacingInventory()
	local methods = peripheral.getMethods("front")

	if not methods then
		return false
	end

	for _, v in pairs(methods) do
		if v == "getItemDetail" then
			return true
		end
	end

	return false
end

local function waitForKeyPress(key)
	local keyPressed = false

	repeat
		local e, p1 = os.pullEvent()

		if e == "key" and p1 == key then
			keyPressed = true
		end
	until keyPressed
end

local function unloadItems()
	print( "Unloading items..." )

	local unloaded = 0
	local fuelSlotsFound = 0

	for slot = 1, 16 do
		local item = turtle.getItemDetail(slot, true)

		if item ~= nil then
			if fuelSlotsFound < 3 and isFuel(item) then
				fuelSlotsFound = fuelSlotsFound + 1
			else
				unloaded = unloaded + item.count
				turtle.select(slot)

				local firstAttempt = true

				while not turtle.drop() do
					if firstAttempt then
						print("Unable to unload items into inventory! Please make room and press SPACE.")
					else
						print("There is still not enough room! Please make room in the unload inventory and press SPACE.")
					end

					firstAttempt = false
					waitForKeyPress(KEYS.SPACE)
				end
			end
		end
	end

	turtle.select(1)

	return unloaded
end

local function checkQuitKey()
	local e, p1

	os.startTimer(0.25)

	while e ~= "char" and e ~= "timer" do
		e, p1 = os.pullEvent()
	end

	if e == "char" and p1:lower() == "q" then
		print("Terminating and returning home...")
		return true
	end

	return false
end

local function doAction(direction, action)
	if not direction then error("Direction is required") end

	validateDirection(direction)
	validateAction(action)

	if direction == "forward" then
		return turtle[action]()
	elseif direction == "back" then
		turtle.turnLeft()
		turtle.turnLeft()

		local result = turtle[action]()

		turtle.turnLeft()
		turtle.turnLeft()

		return result
	elseif direction == "up" then
		return turtle[action .. "Up"]()
	else
		return turtle[action .. "Down"]()
	end
end

local function dig(direction)
	return doAction(direction, "dig")
end

local function attack(direction)
	return doAction(direction, "attack")
end

local function detect(direction)
	return doAction(direction, "detect")
end

local function doMove(direction)
	if not direction then error("Direction is required") end

	validateDirection(direction)

	local didQuit = false
	local moveSuccess = false
	local moveResult

	parallel.waitForAll(
		function()
			didQuit = checkQuitKey()
		end,
		function()
			moveSuccess, moveResult = turtle[direction]()
		end
	)

	return moveSuccess, moveResult, didQuit
end

local function waitForFuel(fuelNeeded)
	print("Waiting for fuel...")

	while turtle.getFuelLevel() < fuelNeeded do
		for slot = 1, 16 do
			local item = turtle.getItemDetail(slot, true)

			turtle.select(slot)


			if item ~= nil and isFuel(item) then
				turtle.refuel(turtle.getItemCount(slot))
			end
		end
	end
end

-- Classes

---- Digger
local Digger = {}

------ Constructor
setmetatable(Digger, {
	__call = function(_)
		local obj = {
			xPos = 0,
			yPos = 0,
			zPos = 0,

			xDir = 0,
			zDir = 1,

			lastItemCount = 0,
			totalMined = 0,
			totalCollected = 0,
			quitRequested = false,
		}

		setmetatable(obj, Digger)

		return obj
	end
})

Digger.__index = Digger

------ Methods
function Digger:getNeededFuelToReturn()
	return math.abs(self.xPos + self.zPos + self.yPos + 4)
end

function Digger:refuel(amount)
	local fuelLevel = turtle.getFuelLevel()

	if fuelLevel == "unlimited" then
		return true
	end

	local needed = amount or self:getNeededFuelToReturn()

	if turtle.getFuelLevel() < needed then
		for slot = 1, 16 do
			if turtle.getItemCount(slot) > 0 then
				turtle.select(slot)

				if turtle.refuel(1) then
					self.lastItemCount = self.lastItemCount - 1 -- subtract the fuel we just ate

					while turtle.getItemCount(slot) > 0 and turtle.getFuelLevel() < needed do
						turtle.refuel(1)
						self.lastItemCount = self.lastItemCount - 1 -- subtract the fuel we just ate
					end

					if turtle.getFuelLevel() >= needed then
						turtle.select(1)

						return true
					end
				end
			end
		end

		turtle.select(1)

		return false
	end

	return true
end

function Digger:turnLeft()
	turtle.turnLeft()
	self.xDir, self.zDir = -self.zDir, self.xDir
end

function Digger:turnRight()
	turtle.turnRight()
	self.xDir, self.zDir = self.zDir, -self.xDir
end

function Digger:updateCollected()
	local totalItemCount = getTotalItemCount()

	self.totalCollected = self.totalCollected + math.max(0, totalItemCount - self.lastItemCount)
	self.lastItemCount = totalItemCount
end

function Digger:tryMove(direction, skipReturn)
	if not direction then error("Direction is required") end

	validateDirection(direction)

	if not self:refuel() then
		if skipReturn then
			local currentFuel = turtle.getFuelLevel()
			local neededFuel = self:getNeededFuelToReturn()

			print("Not enough fuel. Have " .. currentFuel .. " but need " .. neededFuel .. ".")
			waitForFuel()
		else
			print("Not enough fuel to continue. Returning home for more...")
			self:returnSupplies()
			print("Done returning supplies after refueling")
		end
	end

	local attempts = 0
	local printedMoveMessage = false
	local moveSuccess, moveResult, didQuit = doMove(direction)

	while not moveSuccess and not didQuit do
		attempts = attempts + 1

		if attempts > 5 and not printedMoveMessage then
			print("Can't move: " .. moveResult)
			printedMoveMessage = true
		end

		if detect(direction) then
			if dig(direction) then
				self.totalMined = self.totalMined + 1
				self:updateCollected()

				if not skipReturn and not hasRoom() then
					print("Ran out of room after digging")
					self:returnSupplies()
					print("Done returning supplies after digging")
				end
			else
				print("Could not clear obstruction in front of me!")
				return false
			end
		elseif attack(direction) then
			self:updateCollected()

			if not skipReturn and not hasRoom() then
				print("Ran out of room after killing")
				self:returnSupplies()
				print("Done returning supplies after killing")
			end
		else
			sleep(0.5)
		end

		moveSuccess, moveResult, didQuit = doMove(direction)
	end

	self.quitRequested = self.quitRequested or didQuit

	if direction == "forward" then
		self.xPos = self.xPos + self.xDir
		self.zPos = self.zPos + self.zDir
	elseif direction == "back" then
		self.xPos = self.xPos - self.xDir
		self.zPos = self.zPos - self.zDir
	elseif direction == "up" then
		self.yPos = self.yPos + 1
	else
		self.yPos = self.yPos - 1
	end

	return true
end

function Digger:turnTo(xDir, zDir)
	-- If nothing is to be done, just return
	if xDir == self.xDir and zDir == self.zDir then return end

	if math.abs(xDir) > 1 then error("Invalid x-direction: " .. xDir) end
	if math.abs(zDir) > 1 then error("Invalid z-direction: " .. zDir) end
	if xDir == 0 and zDir == 0 then error("One axis must have a magnitude") end
	if xDir == 1 and zDir == 1 then error("One axis must be zero") end

	if (xDir == 0 and self.xDir == 0) or (zDir == 0 and self.zDir == 0) then
		-- Remaining facing along the same axis, just need to turn 180

		self:turnLeft()
		self:turnLeft()
	else
		-- Need to rotate 90 degrees in one direction

		local turnLeft

		if xDir == 0 then -- self.zDir must be 0 right now
			turnLeft = sign(zDir) == sign(self.xDir)
		else -- zDir == 0, self.xDir must be 0 right now
			turnLeft = sign(xDir) ~= sign(self.zDir)
		end

		if turnLeft then
			self:turnLeft()
		else
			self:turnRight()
		end
	end
end

function Digger:goTo(x, y, z, xDir, zDir, moveOrder, skipReturn)
	local moveActions = {
		x = function()
			if self.xPos > x then
				self:turnTo(-1, 0)

				while self.xPos > x do
					if not self:tryMove("forward", skipReturn) then
						sleep(0.5)
					end
				end
			elseif self.xPos < x then
				self:turnTo(1, 0)

				while self.xPos < x do
					if not self:tryMove("forward", skipReturn) then
						sleep(0.5)
					end
				end
			end
		end,
		y = function()
			while self.yPos < y do
				if not self:tryMove("up", skipReturn) then
					sleep(0.5)
				end
			end

			while self.yPos > y do
				if not self:tryMove("down", skipReturn) then
					sleep(0.5)
				end
			end
		end,
		z = function()
			if self.zPos > z then
				self:turnTo(0, -1)

				while self.zPos > z do
					if not self:tryMove("forward", skipReturn) then
						sleep(0.5)
					end
				end
			elseif self.zPos < z then
				self:turnTo(0, 1)

				while self.zPos < z do
					if not self:tryMove("forward", skipReturn) then
						sleep(0.5)
					end
				end
			end
		end,
	}
	local moves = validateMoveOrder(moveOrder or "zxy")

	for m = 1, 3 do
		moveActions[moves[m]]()
	end

	self:turnTo(xDir or self.xDir, zDir or self.zDir)
end

function Digger:returnSupplies()
	local x, y, z, xd, zd = self.xPos, self.yPos, self.zPos, self.xDir, self.zDir

	print("Returning to surface...")
	self:goTo(0, 0, 0, 0, -1, nil, true)

	local fuelNeeded = (x + y + z) * 2 + 4
	local fullyFueled = self:refuel(fuelNeeded)

	while getTotalItemCount() > 0 and not isFacingInventory() do
		print(
			"I have items to return but there's no inventory in front of me. Please empty the items manually," ..
			"or place an inventory in front of me so I can unload. Press SPACE when done."
		)
		waitForKeyPress(KEYS.SPACE)
	end

	unloadItems()

	if not fullyFueled then
		waitForFuel(fuelNeeded)
	end

	self.lastItemCount = getTotalItemCount()
	print("Resuming mining...")
	self:goTo(x, y, z, xd, zd, "yxz", true)
end

-- Exports
M.isFacingInventory = isFacingInventory
M.isFuel = isFuel
M.unloadItems = unloadItems
M.Digger = Digger

return M