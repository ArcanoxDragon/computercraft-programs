local turbineLib = require("turbineLib")
local turbineMonitor = require("turbineMonitor")

local tArgs = { ... }

local turbineName = "back"
local monitorName = "left"
local debugScreenName = "none"
local saveState = false
local autoPower = false

local defaultConfig = {
	monitorName = "left",
	turbineName = "back",
	saveState = true,
	autoPower = true
}
local configFile

if #tArgs >= 2 then
	turbineName = tArgs[1]
	monitorName = tArgs[2]
elseif #tArgs ~= 0 then
	print("Usage: turbineControl [<turbine name> <monitor name>]")
	return
else
	local config = require("config")
	
	configFile = config.open("turbineControl.cfg", defaultConfig)
	monitorName = configFile["monitorName"]
	turbineName = configFile["turbineName"]
	saveState = configFile["saveState"]
	autoPower = configFile["autoPower"]
	debugScreenName = configFile["debugScreenName"] or "none"
end

if debugScreenName == "current" and monitorName == "current" then
	print("Cannot debug on main monitor!")
	return
end

local _debug = debugScreenName ~= "none"

if peripheral.getType(turbineName) ~= "bigger-turbine" then
	print("Device specified by turbineName (" .. turbineName .. ") is not a turbine.")
	return
end

local turbinePeripheral = peripheral.wrap(turbineName)
if not turbinePeripheral then
	print("Unable to wrap turbine!")
	return
end

local monitor = nil
if monitorName ~= "none" and monitorName ~= "current" then
	if peripheral.getType(monitorName) ~= "monitor" then
		print("Device specified by monitorName (" .. monitorName .. ") is not a monitor.")
		return
	end
	monitor = peripheral.wrap(monitorName)
	if not monitor then
		print("Unable to wrap monitor!")
		return
	end
	monitor.setTextScale(0.5)
end

local monitorUpdateTick = 0

local debugMon = nil
if _debug then
	if debugScreenName ~= "none" and debugScreenName ~= "current" then
		if peripheral.getType(debugScreenName) ~= "monitor" then
			print("Device specified by debugScreenName (" .. debugScreenName .. ") is not a monitor.")
			return
		end
		debugMon = peripheral.wrap(debugScreenName)
		if not debugMon then
			print("Unable to wrap debug monitor!")
			return
		end
		debugMon.setTextScale(0.5)
	end
end

local turbine = turbineLib.Turbine(turbinePeripheral, {
	kP = configFile["pidKP"],
	kI = configFile["pidKI"],
	kD = configFile["pidKD"],
	pidScale = configFile["pidScale"],
	startupFlowRate = configFile["startupFlowRate"],
	maxFlowRate = configFile["maxFlowRate"],
})
local turbineMon = turbineMonitor.TurbineMonitor(turbine)

turbine.autoPower = autoPower

if saveState then
	if fs.exists("turbineState") then
		local f = fs.open("turbineState", "rb")
		turbine.turbineOn = (f.read() or 0) > 0
		turbine.curState = turbine.turbineOn and turbineLib.STATE_STARTING or turbineLib.STATE_STOPPED
		turbine.lastCoilState = turbine.curState ~= turbineLib.STATE_STOPPED
		turbine.targetRPM = ((f.read() or 0) > 0) and 1800 or 900
		f.close()
	else
		local f = fs.open("turbineState", "wb")
		f.write(0)
		f.write(0)
		f.close()
	end
end

term.clear()
term.setCursorPos(1, 1)
print("Monitoring turbine...")
if monitor then
	print("See adjacent monitor for detailed information")
end

local function updateMonitor()
	if monitorName == "none" then return end
	
	local curTerm = term.current()
	
	if monitor then
		term.redirect(monitor)
	end
	
	if not term.isColor() then
		term.redirect(curTerm)
		return
	end
	
	term.setBackgroundColor(colors.black)
	term.clear()
	
	turbineMon:drawRPMStatus(2, 2)
	turbineMon:drawFlowStatus(2, 7)
	turbineMon:drawTurbineStatus(2, 13)
	
	term.setCursorPos(2, 15)
	term.setTextColor(colors.white)
	term.write("Target RPM (click to change):")
	
	turbineMon:drawButtons()
	
	term.redirect(curTerm)
end

local function onClickStartButton(button)
	turbine.turbineOn = not turbine.turbineOn
	button.color = turbine.turbineOn and colors.green or colors.red
	if turbine.turbineOn then
		button.text = "STOP"
		turbine.curState = turbineLib.STATE_STARTING
	else
		button.text = "START"
		turbine.curState = turbineLib.STATE_STOPPED
		turbine.lastCoilState = false
	end
	
	if saveState then
		local f = fs.open("turbineState", "wb")
		f.write(turbine.turbineOn and 1 or 0)
		f.write((turbine.targetRPM == 1800) and 1 or 0)
		f.close()
	end
end

local function onClickRPMButton(button)
	if turbine.targetRPM == 900 then
		turbine.targetRPM = 1800
		if turbine.curState == turbineLib.STATE_RUNNING then
			turbine.curState = turbineLib.STATE_STARTING
			turbine.lastCoilState = false
		end -- Kill the inductors so that it reaches speed faster
	else
		turbine.targetRPM = 900
	end
	button.text = tostring(turbine.targetRPM) .. " RPM"
	
	if saveState then
		local f = fs.open("turbineState", "wb")
		f.write(turbine.turbineOn and 1 or 0)
		f.write((turbine.targetRPM == 1800) and 1 or 0)
		f.close()
	end
end

if ((monitor and monitor.isColor()) or term.isColor()) and monitorName ~= "none" then
	local curTerm = term.current()
	
	if monitor then
		term.redirect(monitor) -- Need to have active color support for buttons
	end
	
	local sW, sH = term.getSize()
	
	-- Start/Stop button:
	turbineMon:addButtonToScreen(turbineMonitor.newButton(sW - 15, 2, 14, 3, turbine.turbineOn and "STOP" or "START", onClickStartButton, 
		turbine.turbineOn and colors.green or colors.red))

	-- RPM button
	turbineMon:addButtonToScreen(turbineMonitor.newButton(2, 16, 16, 3, tostring(turbine.targetRPM) .. " RPM", onClickRPMButton, colors.blue))
	
	term.redirect(curTerm)
end

local mouseEvent = monitor and "monitor_touch" or "mouse_click"
local loopTimer
local e, a1, a2, a3

while true do
	loopTimer = os.startTimer(turbine.tickInterval)
	e, a1, a2, a3 = os.pullEvent()
	
	if e == "timer" and a1 == loopTimer then
		turbine:update()
		
		if configFile["pidDebug"] then
			term.setCursorPos(1, 3)
			term.clearLine()
			print("E: " .. turbine.pid.err)
			term.clearLine()
			print("P: " .. turbine.pid.pFactor)
			term.clearLine()
			print("I: " .. turbine.pid.iFactor)
			term.clearLine()
			print("D: " .. turbine.pid.dFactor)
		end
		
		monitorUpdateTick = monitorUpdateTick + 1
		
		if (monitorUpdateTick >= 5) then
			monitorUpdateTick = 0
			updateMonitor()
		end
	elseif e == mouseEvent then
		local x, y = a2, a3
		turbineMon:handleClickEvent(x, y)
	end
	
	os.cancelTimer(loopTimer)
end