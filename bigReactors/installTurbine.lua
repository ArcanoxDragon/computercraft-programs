local updaterURL = "https://bitbucket.org/ArcanoxDragon/computercraft-programs/raw/master/updater.lua"
local updaterCfgURL = "https://bitbucket.org/ArcanoxDragon/computercraft-programs/raw/master/bigReactors/prefab/updaterTurbineOnly.cfg"

local updaterFile = http.get(updaterURL)
local updaterCfgFile = http.get(updaterCfgURL)

local updaterText = updaterFile.readAll()
local updaterCfgText = updaterCfgFile.readAll()

updaterFile.close()
updaterCfgFile.close()

local turbineSide = rs.getSides()[1]
local monitorSide = rs.getSides()[2]
local pType

for _,side in pairs(rs.getSides()) do
	pType = peripheral.getType(side)
	if pType == "BigReactors-Turbine" then
		turbineSide = side
	elseif pType == "monitor" then
		monitorSide = side
	end
end

local turbineCfgText = "saveState=true\nautoPower=true\nmonitorName=" .. monitorSide .. "\nturbineName=" .. turbineSide .. "\n"
	
local turbineCfgFile = fs.open("/turbineControl.cfg", "w")
updaterFile = fs.open("/updater.lua", "w")
updaterCfgFile = fs.open("/updater.cfg", "w")

turbineCfgFile.write(turbineCfgText)
updaterFile.write(updaterText)
updaterCfgFile.write(updaterCfgText)

turbineCfgFile.close()
updaterFile.close()
updaterCfgFile.close()

shell.run("/updater.lua")
os.reboot()