local util = require("arc_util")

local tArgs = { ... }

local reactorName = "back"
local monitorName = "left"
local saveState = false

local defaultConfig = {
	monitorName = "left",
	reactorName = "back",
	saveState = true
}
local configFile

if #tArgs >= 2 then
	reactorName = tArgs[1]
	monitorName = tArgs[2]
elseif #tArgs ~= 0 then
	print("Usage: reactorControl [<reactor name> <monitor name>]")
	return
else
	local config = require("config")

	configFile = config.open("reactorControl.cfg", defaultConfig)
	monitorName = configFile["monitorName"]
	reactorName = configFile["reactorName"]
	saveState = configFile["saveState"]
end

local reactorPeripheral = peripheral.wrap(reactorName)
local monitor = peripheral.wrap(monitorName)

if reactorPeripheral == nil or (monitor == nil and monitorName ~= "none") then
	print("Unable to wrap reactor or monitor! Exiting.")
	return
end

if monitor ~= nil then
	monitor.setTextScale(0.5)
end

if monitor == nil then
	monitor = term
end

local buttons = {}
local monitorUpdateTick = 0
local reactorLib = require("reactorLib")
local reactor = reactorLib.Reactor(reactorPeripheral, {
	kP = configFile["pidKP"],
	kI = configFile["pidKI"],
	kIOut = configFile["pidKIOut"],
	kD = configFile["pidKD"],
	pidScale = configFile["pidScale"],
})

if saveState then
	if fs.exists("reactorState") then
		local f = fs.open("reactorState", "rb")
		reactor.reactorOn = (f.read() or 0) > 0
		f.close()
	else
		local f = fs.open("reactorState", "wb")
		f.write(0)
		f.close()
	end
end

local MAX_COOLANT_DIGITS = tostring(reactor.maxCoolant):len()
local MAX_HEAT_DIGITS = tostring(reactorLib.MAX_HEAT):len()
local MAX_STEAM_DIGITS = tostring(reactor.maxSteam):len()
local MAX_POWER_DIGITS = tostring(reactorLib.MAX_POWER / 1000):len()

term.clear()
term.setCursorPos(1, 1)
print("Monitoring reactor...")
print("See adjacent monitor for detailed information")

local function newButton(x, y, width, height, text, onClick, color)
	local button = {}
	button["x"] = x
	button["y"] = y
	button["width"] = width
	button["height"] = height
	button["text"] = text
	button["onClick"] = onClick
	button["color"] = color or colors.red
	return button
end

local function addButtonToScreen(button)
	buttons[#buttons + 1] = button
end

local function drawButton(button)
	monitor.setBackgroundColor(button.color)
	for i = button.y, button.y + button.height - 1 do
		monitor.setCursorPos(button.x, i)
		monitor.write(string.rep(" ", button.width))
	end
	local cX = button.x + ((button.width - button.text:len()) / 2)
	local cY = button.y + (button.height / 2)
	monitor.setCursorPos(cX, cY)
	monitor.setTextColor(colors.white)
	monitor.setBackgroundColor(button.color)
	monitor.write(button.text)
end

local function drawCoolantStatus(x, y)
	monitor.setCursorPos(x, y)
	monitor.setTextColor(colors.white)
	monitor.write("Coolant:")
	local coolantColor
	if reactor.coolant < reactor.minCoolant then
		coolantColor = colors.red
	elseif reactor.coolant < reactor.maxCoolant / 2 then
		coolantColor = colors.yellow
	else
		coolantColor = colors.green
	end
	local coolantBarMaxLen = MAX_COOLANT_DIGITS * 2 + 6 -- 6 extra for the slash and the "mB" label
	local coolantBarCurLen = util.round((reactor.coolant / reactor.maxCoolant) * coolantBarMaxLen)
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(colors.gray)
	monitor.write(string.rep(" ", coolantBarMaxLen))
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(coolantColor)
	monitor.write(string.rep(" ", coolantBarCurLen))
	monitor.setBackgroundColor(colors.black)
	monitor.setTextColor(coolantColor)
	monitor.setCursorPos(x, y + 2)
	monitor.write(tostring(reactor.coolant))
	monitor.setTextColor(colors.white)
	monitor.setCursorPos(x + MAX_COOLANT_DIGITS, y + 2)
	monitor.write(" / ")
	monitor.setTextColor(colors.green)
	monitor.write(tostring(reactor.maxCoolant))
	monitor.setTextColor(colors.white)
	monitor.write(" mB")
end

local function drawPowerStatus(x, y)
	monitor.setCursorPos(x, y)
	monitor.setTextColor(colors.white)
	monitor.write("Power Production:")
	local powerColor
	if reactor.power < 500 then
		powerColor = colors.red
	elseif reactor.power < 1500 / 2 then
		powerColor = colors.yellow
	else
		powerColor = colors.green
	end
	monitor.setCursorPos(x, y + 2)
	monitor.setBackgroundColor(colors.black)
	monitor.setTextColor(powerColor)
	monitor.write(tostring(util.round(reactor.power)))
	monitor.setCursorPos(x + 6, y + 2)
	monitor.setTextColor(colors.white)
	monitor.write(" RF/t")
end

local function drawHeatStatus(x, y)
	monitor.setCursorPos(x, y)
	monitor.setTextColor(colors.white)
	monitor.write("Temperature:")
	local heatColor
	if reactor.heat > reactorLib.MAX_HEAT then
		heatColor = colors.red
	elseif reactor.heat > reactorLib.MAX_HEAT / 2 then
		heatColor = colors.yellow
	else
		heatColor = colors.green
	end
	local heatBarMaxLen = MAX_HEAT_DIGITS * 2 + 6 -- 6 extra for the slash and the "'C" label
	local heatBarCurLen = util.round(math.min(reactor.heat / reactorLib.MAX_HEAT, 1.0) * heatBarMaxLen)
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(colors.gray)
	monitor.write(string.rep(" ", heatBarMaxLen))
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(heatColor)
	monitor.write(string.rep(" ", heatBarCurLen))
	monitor.setBackgroundColor(colors.black)
	monitor.setTextColor(heatColor)
	monitor.setCursorPos(x, y + 2)
	monitor.write(tostring(util.round(reactor.heat)))
	monitor.setTextColor(colors.white)
	monitor.setCursorPos(x + MAX_HEAT_DIGITS, y + 2)
	monitor.write(" / ")
	monitor.setTextColor(colors.green)
	monitor.write(tostring(reactorLib.MAX_HEAT))
	monitor.setTextColor(colors.white)
	monitor.write(" 'C")
end

local function drawSteamStatus(x, y)
	monitor.setCursorPos(x, y)
	monitor.setTextColor(colors.white)
	monitor.write("Hot Fluid (" .. util.properCase(reactor.getHotFluidType() or "none") .. "):")
	local steamColor
	if reactor.steam >= reactor.targetSteam - 50 then
		steamColor = colors.blue
	elseif reactor.steam >= reactor.targetSteam - 250 then
		steamColor = colors.green
	else
		steamColor = colors.yellow
	end
	local steamBarMaxLen = MAX_STEAM_DIGITS * 2 + 6 -- 6 extra for the slash and the "mB" label
	local steamBarCurLen = util.round(math.min(reactor.steam / reactor.maxSteam, 1.0) * steamBarMaxLen)
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(colors.gray)
	monitor.write(string.rep(" ", steamBarMaxLen))
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(steamColor)
	monitor.write(string.rep(" ", steamBarCurLen))
	monitor.setBackgroundColor(colors.black)
	monitor.setTextColor(steamColor)
	monitor.setCursorPos(x, y + 2)
	monitor.write(tostring(util.round(reactor.steam)))
	monitor.setTextColor(colors.white)
	monitor.setCursorPos(x + MAX_STEAM_DIGITS, y + 2)
	monitor.write(" / ")
	monitor.setTextColor(colors.green)
	monitor.write(tostring(reactor.maxSteam))
	monitor.setTextColor(colors.white)
	monitor.write(" mB")
end

local function drawPowerStoredStatus(x, y)
	monitor.setCursorPos(x, y)
	monitor.setTextColor(colors.white)
	monitor.write("Power Stored:")
	local powerBarMaxLen = MAX_POWER_DIGITS * 2 + 7 -- 6 extra for the slash and the "kRF" label
	local powerBarCurLen = util.round(math.min(reactor.getEnergyStored() / reactorLib.MAX_POWER, 1.0) * powerBarMaxLen)
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(colors.gray)
	monitor.write(string.rep(" ", powerBarMaxLen))
	monitor.setCursorPos(x, y + 3)
	monitor.setBackgroundColor(colors.red)
	monitor.write(string.rep(" ", powerBarCurLen))
	monitor.setBackgroundColor(colors.black)
	monitor.setTextColor(colors.green)
	monitor.setCursorPos(x, y + 2)
	monitor.write(tostring(util.round(reactor.getEnergyStored() / 1000)))
	monitor.setTextColor(colors.white)
	monitor.setCursorPos(x + MAX_POWER_DIGITS, y + 2)
	monitor.write(" / ")
	monitor.setTextColor(colors.green)
	monitor.write(tostring(reactorLib.MAX_POWER / 1000))
	monitor.setTextColor(colors.white)
	monitor.write(" kRF")
end

local function drawControlRod(x, y)
		monitor.setTextColor(colors.white)
		monitor.setCursorPos(x, y)
		monitor.write("Control Rod Level: ")
		if reactor.controlRodLevel < 0.25 then
			monitor.setTextColor(colors.red)
		elseif reactor.controlRodLevel < 0.5 then
			monitor.setTextColor(colors.orange)
		elseif reactor.controlRodLevel < 0.75 then
			monitor.setTextColor(colors.yellow)
		else
			monitor.setTextColor(colors.green)
		end
		monitor.write(tostring(util.round(reactor.controlRodLevel)) .. "%")
		local controlRodMaxLen = 24 -- 6 extra for the slash and the "mB" label
		local controlRodCurLen = util.round(math.min(reactor.controlRodLevel / 100.0, 1.0) * controlRodMaxLen)
		monitor.setCursorPos(x, y + 1)
		monitor.setBackgroundColor(colors.gray)
		monitor.write(string.rep(" ", controlRodMaxLen))
		monitor.setCursorPos(x, y + 1)
		monitor.setBackgroundColor(colors.yellow)
		monitor.write(string.rep(" ", controlRodCurLen))
end

local function drawReactorStatus(x, y)
		monitor.setCursorPos(x, y)
		monitor.write("Reactor Status: ")
		monitor.setTextColor(reactor.getActive() and colors.green or colors.red)
		monitor.write(reactor.getActive() and "ACTIVE" or "IDLE")
end

local function updateMonitor()
	monitor.setBackgroundColor(colors.black)
	monitor.clear()

	if reactor.reactorType == 1 then
		drawCoolantStatus(2, 2)
		drawHeatStatus(2, 7)
		drawSteamStatus(2, 12)
		drawReactorStatus(2, 18)
		drawControlRod(2, 19)
	else
		drawPowerStatus(2, 2)
		drawHeatStatus(2, 6)
		drawPowerStoredStatus(2, 12)
		drawReactorStatus(2, 18)
		drawControlRod(2, 19)
	end

	for i,v in ipairs(buttons) do
		drawButton(v)
	end
end

local sW, sH = monitor.getSize()

-- Enable/Disable button:
addButtonToScreen(newButton(sW - 15, 2, 14, 3, reactor.reactorOn and "DISABLE" or "ENABLE", function(button)
	reactor.reactorOn = not reactor.reactorOn
	button.color = reactor.reactorOn and colors.green or colors.red
	if reactor.reactorOn then button.text = "DISABLE" else button.text = "ENABLE" end

	if saveState then
		local f = fs.open("reactorState", "wb")
		f.write(reactor.reactorOn and 1 or 0)
		f.close()
	end
end, reactor.reactorOn and colors.green or colors.red))

local touchEvent = (monitorName == "none") and "mouse_click" or "monitor_touch"

local loopTimer
local e, a1, a2, a3
while true do
	loopTimer = os.startTimer(0.05)
	e, a1, a2, a3 = os.pullEvent()

	if e == touchEvent then
		local x, y = a2, a3
		for i,v in ipairs(buttons) do
			if x >= v.x and x <= v.x + v.width and y >= v.y and y <= v.y + v.height then
				if v.onClick then v.onClick(v, x - v.x, y - v.y) end
			end
		end
	elseif e == "timer" and a1 == loopTimer then
		reactor:update()

		if configFile["pidDebug"] then
			term.setCursorPos(1, 3)
			term.clearLine()
			print("E: " .. reactor.pid.err)
			term.clearLine()
			print("P: " .. reactor.pid.pFactor)
			term.clearLine()
			print("I: " .. reactor.pid.iFactor)
			term.clearLine()
			print("D: " .. reactor.pid.dFactor)
		end

		monitorUpdateTick = monitorUpdateTick + 1
		if (monitorUpdateTick >= 5) then
			monitorUpdateTick = 0
			updateMonitor()
		end
	end

	os.cancelTimer(loopTimer)
end