local M = {}

local util = require("arc_util")

local STATE_STOPPED = 0
local STATE_STARTING = 1
local STATE_RUNNING = 2
local buttons = {}

local function setTextColor(color)
	if term.isColor() then term.setTextColor(color) end
end

local function setBackgroundColor(color)
	if term.isColor() then term.setBackgroundColor(color) end
end

local function drawButton(button)
	if not term.isColor() then error("Buttons require color support") end
	setBackgroundColor(button.color)
	for i = button.y, button.y + button.height - 1 do
		term.setCursorPos(button.x, i)
		term.write(string.rep(" ", button.width))
	end
	local cX = button.x + ((button.width - button.text:len()) / 2)
	local cY = button.y + (button.height / 2)
	term.setCursorPos(cX, cY)
	setTextColor(colors.white)
	setBackgroundColor(button.color)
	term.write(button.text)
end

function M.newButton(x, y, width, height, text, onClick, color)
	if not term.isColor() then error("Buttons require color support") end
	local button = {}
	button["x"] = x
	button["y"] = y
	button["width"] = width
	button["height"] = height
	button["text"] = text
	button["onClick"] = onClick
	button["color"] = color or colors.red
	return button
end

function M.addButtonToScreen(button)
	if not term.isColor() then error("Buttons require color support (button \"" .. button.text .. "\")") end
	buttons[#buttons + 1] = button
end

function M.drawButtons()
	if not term.isColor() then error("Buttons require color support") end
	for i,v in ipairs(buttons) do
		drawButton(v)
	end
end

function M.drawRPMStatus(x, y, rpm, targetRPM, maxRPM)
	local maxRPMDigits = tostring(maxRPM):len()
	term.setCursorPos(x, y)
	setTextColor(colors.white)
	term.write("RPM:")
	local rpmColor
	local closestRPMRange = math.abs(rpm - targetRPM)
	if closestRPMRange < 25 then
		rpmColor = colors.blue
	elseif closestRPMRange < 75 then
		rpmColor = colors.green
	else
		rpmColor = colors.yellow
	end
	local rpmBarMaxLen = maxRPMDigits * 2 + 7 -- 7 extra for the slash and the "RPM" label
	local rpmBarCurLen = util.round((rpm / maxRPM) * rpmBarMaxLen)
	if term.isColor() then
		term.setCursorPos(x, y + 3)
		setBackgroundColor(colors.gray)
		term.write(string.rep(" ", rpmBarMaxLen))
	end
	term.setCursorPos(x, y + 3)
	setBackgroundColor(rpmColor)
	term.write(string.rep(" ", rpmBarCurLen))
	setBackgroundColor(colors.black)
	setTextColor(rpmColor)
	term.setCursorPos(x, y + 2)
	term.write(tostring(util.round(rpm)))
	setTextColor(colors.white)
	term.setCursorPos(x + maxRPMDigits, y + 2)
	term.write(" / ")
	setTextColor(colors.blue)
	term.write(tostring(maxRPM))
	setTextColor(colors.white)
	term.write(" RPM")
end

function M.drawFlowStatus(x, y, flowRate, maxFlowRate)
	local maxFlowRateDigits = tostring(maxFlowRate):len()
	term.setCursorPos(x, y)
	setTextColor(colors.white)
	term.write("Flow Rate:")
	local flowBarMaxLen = maxFlowRateDigits * 2 + 8 -- 8 extra for the slash and the "mB/t" label
	local flowBarCurLen = util.round((flowRate / maxFlowRate) * flowBarMaxLen)
	if term.isColor() then
		term.setCursorPos(x, y + 3)
		setBackgroundColor(colors.gray)
		term.write(string.rep(" ", flowBarMaxLen))
	end
	term.setCursorPos(x, y + 3)
	setBackgroundColor(colors.blue)
	term.write(string.rep(" ", flowBarCurLen))
	setBackgroundColor(colors.black)
	setTextColor(colors.blue)
	term.setCursorPos(x, y + 2)
	term.write(tostring(flowRate))
	setTextColor(colors.white)
	term.setCursorPos(x + maxFlowRateDigits, y + 2)
	term.write(" / ")
	setTextColor(colors.blue)
	term.write(tostring(maxFlowRate))
	setTextColor(colors.white)
	term.write(" mB/t")
end

function M.drawTurbineStatus(x, y, state)
	term.setCursorPos(x, y)
	term.write("Turbine Status: ")
	if state == STATE_STOPPED then
		setTextColor(colors.red)
		term.write("STOPPED")
	elseif state == STATE_STARTING then
		setTextColor(colors.yellow)
		term.write("STARTING")
	else
		setTextColor(colors.green)
		term.write("RUNNING")
	end
end

function M.handleClickEvent(x, y)
	for i,v in ipairs(buttons) do
		if x >= v.x and x <= v.x + v.width and y >= v.y and y <= v.y + v.height then
			if v.onClick ~= nil then v.onClick(v, x - v.x, y - v.y) end
		end
	end
end

return M