local updaterURL = "https://bitbucket.org/ArcanoxDragon/computercraft-programs/raw/master/updater.lua"
local updaterCfgURL = "https://bitbucket.org/ArcanoxDragon/computercraft-programs/raw/master/bigReactors/prefab/updaterReactorOnly.cfg"

local updaterFile = http.get(updaterURL)
local updaterCfgFile = http.get(updaterCfgURL)

local updaterText = updaterFile.readAll()
local updaterCfgText = updaterCfgFile.readAll()

updaterFile.close()
updaterCfgFile.close()

local reactorSide = rs.getSides()[1]
local monitorSide = rs.getSides()[2]
local pType

for _,side in pairs(rs.getSides()) do
	pType = peripheral.getType(side)
	if pType == "BigReactors-Reactor" then
		reactorSide = side
	elseif pType == "monitor" then
		monitorSide = side
	end
end

local reactorCfgText = "saveState=true\nmonitorName=" .. monitorSide .. "\nreactorName=" .. reactorSide .. "\n"
	
local reactorCfgFile = fs.open("/reactorControl.cfg", "w")
updaterFile = fs.open("/updater.lua", "w")
updaterCfgFile = fs.open("/updater.cfg", "w")

reactorCfgFile.write(reactorCfgText)
updaterFile.write(updaterText)
updaterCfgFile.write(updaterCfgText)

reactorCfgFile.close()
updaterFile.close()
updaterCfgFile.close()

shell.run("/updater.lua")
os.reboot()