local M = {}

local DEFAULT_PID_KP = 0.5
local DEFAULT_PID_KI = 0.35
local DEFAULT_PID_KD = 0.15
local DEFAULT_STARTUP_FLOW_RATE = 2000

M.MAX_RPM = 2000
M.MAX_ENERGY = 2500000
M.HIGH_ENERGY_THRESHOLD = M.MAX_ENERGY * 0.95
M.LOW_ENERGY_THRESHOLD = M.MAX_ENERGY * 0.25
M.STATE_STOPPED = 0
M.STATE_STARTING = 1
M.STATE_RUNNING = 2

local pidLib = require("pid")
local util = require("arc_util")

local Turbine = {}

Turbine.mt = {}
Turbine.__index = function(table, key)
	if key == "peripheral" then
		return nil
	end
	if table.peripheral == nil then
		return nil
	end
	return table.peripheral[key] or Turbine[key]
end

Turbine.mt.__call = function(table, peripheral, config)
	config = config or {}
	config.kP = config.kP or DEFAULT_PID_KP
	config.kI = config.kI or DEFAULT_PID_KI
	config.kD = config.kD or DEFAULT_PID_KD
	config.pidScale = config.pidScale or 0.001
	config.startupFlowRate = config.startupFlowRate or DEFAULT_STARTUP_FLOW_RATE

	if peripheral == nil then -- mandatory
		return nil
	end

	local turbine = {}
	setmetatable(turbine, Turbine)

	turbine.config = config
	turbine.peripheral = peripheral
	turbine.maxFlow = config.maxFlowRate or peripheral.getFluidFlowRateMaxMax()
	turbine.rpm = 0
	turbine.flow = 0
	turbine.energy = 0
	turbine.turbineOn = false
	turbine.lastCoilState = false
	turbine.curFlowRate = 0.0
	turbine.targetRPM = 900
	turbine.lastRS = false
	turbine.startupPower = 0
	turbine.autoPower = false
	turbine.maxStartingFlow = config.startupFlowRate
	turbine.curState = M.STATE_STOPPED
	turbine.lastState = M.STATE_STOPPED
	turbine.tickInterval = 0.05
	turbine.pid = pidLib.PID({
		kP = config.kP or DEFAULT_PID_KP,
		kI = config.kI or DEFAULT_PID_KI,
		kD = config.kD or DEFAULT_PID_KD,
		scale = config.pidScale,
		minIntegral = -1,
		maxIntegral = 1,
		minOutput = 0,
		maxOutput = 1,
	})

	return turbine
end

function Turbine:update()
	self.rpm = self.getRotorSpeed()
	self.flow = self.getFluidFlowRate()
	self.energy = self.getEnergyStored()

	if self.curState == M.STATE_RUNNING then
		local pidOutput = self.pid:get(self.targetRPM, self.rpm, 0.05) -- TODO: Dynamically calculate dt?

		self.curFlowRate = pidOutput * self.maxFlow
		self.setActive(true)
		self.setFluidFlowRateMax(self.curFlowRate)

		if self.autoPower then
			if self.energy < M.LOW_ENERGY_THRESHOLD then
				self.setInductorEngaged(true)
			elseif self.energy > M.HIGH_ENERGY_THRESHOLD then
				self.setInductorEngaged(false)
			end
		else
			self.setInductorEngaged(true)
		end
		self.startupPower = 0
	elseif self.curState == M.STATE_STARTING then
		self.setActive(true)
		self.setFluidFlowRateMax(self.startupPower)
		self.startupPower = math.min(self.startupPower + 10, self.maxStartingFlow)
		self.setInductorEngaged(false)

		if self.rpm > self.targetRPM - 50 then
			self.curState = M.STATE_RUNNING
			self.lastCoilState = true
		end

		self.pid:reset()
	else
		self.setActive(self.rpm > 0)
		self.setFluidFlowRateMax(0.0)
		self.setInductorEngaged(self.rpm > 0) -- Helps slow down the rotors
		self.startupPower = 0
		self.pid:reset()
	end

	self.lastState = self.curState
end

setmetatable(Turbine, Turbine.mt)

M.Turbine = Turbine

return M