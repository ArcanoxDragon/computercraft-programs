local M = {}

local DEFAULT_PID_KP_TYPE0 = 0.75
local DEFAULT_PID_KI_TYPE0 = 0.35
local DEFAULT_PID_KD_TYPE0 = 0.02
local DEFAULT_PID_SCALE_TYPE0 = 1
local DEFAULT_PID_KP_TYPE1 = 0.10
local DEFAULT_PID_KI_TYPE1 = 0.10
local DEFAULT_PID_KD_TYPE1 = 0.02
local DEFAULT_PID_SCALE_TYPE1 = 1.0 / 100.0

local util = require("arc_util")
local pidLib = require("pid")

M.MAX_HEAT = 1750
M.MAX_POWER = 10000000 -- 10 million (constant)

local Reactor = {}

Reactor.mt = {}
Reactor.__index = function(table, key)
	if key == "peripheral" then
		return nil
	end
	if table.peripheral == nil then
		return nil
	end
	return table.peripheral[key] or Reactor[key]
end

Reactor.mt.__call = function(table, peripheral, config)
	config = config or {}

	if peripheral == nil then -- mandatory
		return nil
	end

	local reactor = {}
	setmetatable(reactor, Reactor)

	reactor.peripheral = peripheral
	reactor.minCoolant = 500 -- mB
	reactor.maxCoolant = peripheral.getCoolantAmountMax()
	reactor.maxSteam = peripheral.getHotFluidAmountMax()
	reactor.targetSteam = reactor.maxSteam / 2.0
	reactor.targetPower = M.MAX_POWER / 2.0
	reactor.reactorType = (reactor.maxCoolant == 0) and 0 or 1 -- 1 == steam reactor, 0 == regular power-only reactor
	reactor.coolant = 0
	reactor.power = 0
	reactor.steam = 0
	reactor.heat = 0
	reactor.correct = 0
	reactor.controlRodLevel = 0
	reactor.reactorOn = false

	if reactor.reactorType == 1 then
		config.kP = config.kP or DEFAULT_PID_KP_TYPE1
		config.kI = config.kI or DEFAULT_PID_KI_TYPE1
		config.kD = config.kD or DEFAULT_PID_KD_TYPE1
		config.pidScale = config.pidScale or DEFAULT_PID_SCALE_TYPE1
	else
		config.kP = config.kP or DEFAULT_PID_KP_TYPE0
		config.kI = config.kI or DEFAULT_PID_KI_TYPE0
		config.kD = config.kD or DEFAULT_PID_KD_TYPE0
		config.pidScale = config.pidScale or DEFAULT_PID_SCALE_TYPE0
	end

	reactor.pid = pidLib.PID({
		kP = config.kP,
		kI = config.kI,
		kIOut = config.kIOut,
		kD = config.kD,
		scale = config.pidScale,
		minOutput = 0,
		maxOutput = 1,
	})

	return reactor
end

function Reactor:update()
	if self.reactorType == 1 then
		self.coolant = self.peripheral.getCoolantAmount()
		self.power = 0
		self.steam = self.peripheral.getHotFluidAmount()
	else
		self.coolant = 0
		self.power = self.peripheral.getEnergyProducedLastTick()
		self.steam = 0
	end

	self.heat = math.max(self.peripheral.getFuelTemperature(), self.peripheral.getCasingTemperature())

	local pidOutput

	if self.reactorType == 1 then -- Target steam output
		pidOutput = self.pid:get(self.targetSteam, self.steam, 0.05) -- TODO: Dynamically calculate dt?
	else -- Target power output
		local powerStored = util.clamp(self.peripheral.getEnergyStored(), 0, M.MAX_POWER)

		pidOutput = self.pid:get(self.targetPower, powerStored, 0.05) -- TODO: Dynamically calculate dt?
	end

	self.controlRodLevel = 100 * (1 - pidOutput)
	self.peripheral.setActive(self.reactorOn)

	if not self.reactorOn then
		self.pid:reset()
	end

	self.peripheral.setAllControlRodLevels(self.reactorOn and self.controlRodLevel or 100.0)
end

setmetatable(Reactor, Reactor.mt)

M.Reactor = Reactor

return M