local M = {}

local util = require("arc_util")
local pidDebugger = require("pid_debugger")

local PID = {}

setmetatable(PID, {
	__call = function(_, config)
		config = config or {}

		local obj = {
			kPIn = config.kPIn or config.kP or 0,
			kPOut = config.kPOut or config.kP or 0,
			kIIn = config.kIIn or config.kI or 0,
			kIOut = config.kIOut or config.kI or 0,
			kD = config.kD or 0,
			minOutput = config.minOutput or -math.huge,
			maxOutput = config.maxOutput or math.huge,
			scale = config.scale or 1,
			name = config.name or "pid",
			dThreshold = config.integralLockThreshold,
			dThresholdReset = config.integralLockAutoReset == true,
			debug = config.debug == true,
			output = 0,

			lastValue = nil,
			lastError = nil,
			integral = 0,
			lockIntegral = false,
			dynamicIntegralLock = false,

			-- state variables to avoid a bunch of locals
			target = 0,
			actual = 0,
			err = 0,
			delta = 0,
			pFactor = 0,
			iFactor = 0,
			dFactor = 0,
			integralLocked = 0,
		}

		obj.minIntegral = config.minIntegral or obj.minOutput
		obj.maxIntegral = config.maxIntegral or obj.maxOutput

		setmetatable(obj, PID)
		return obj
	end
})

PID.__index = PID

function PID:update(target, actual, dt)
    self.target = target
    self.actual = actual

    -- calculate early P/D values
    self.err = (target - actual) * self.scale
    self.delta = self.lastError and (self.err - self.lastError) * self.scale or 0
    self.pFactor = (self.err * actual > 0 and self.kPOut or self.kPIn) * self.err
    self.dFactor = self.kD * self.delta / dt

    -- apply integral locking/diff
    self.integralLocked = self.lockIntegral or self.dynamicIntegralLock
    self.dynamicIntegralLock = self.dThreshold and math.abs(self.delta) > self.dThreshold
    self.integral = (self.dynamicIntegralLock and self.dThresholdReset) and 0 or self.integral
    self.integral = self.integralLocked and self.integral or util.clamp(self.integral + self.err * (self.err * actual > 0 and self.kIOut or self.kIIn) * dt, self.minIntegral, self.maxIntegral)

    -- apply integral factor
    self.iFactor = self.integral

    -- set output and last-tick values
    self.output = util.clamp(self.pFactor + self.iFactor - self.dFactor, self.minOutput, self.maxOutput)
    self.lastValue = actual
    self.lastError = self.err
end

function PID:get(target, actual, dt)
    if target and actual and dt then -- all three parameters provided; perform an update too
        self:update(target, actual, dt)
    end

    if self.debug then
        self:printDebug()
    end

    return self.output
end

function PID:peek()
    return self.output
end

function PID:setConfig(config)
    config = config or {}

    for k, v in pairs(config) do
        self[k] = v
    end

    self.debugutil.enabled = self.debug == true
end

function PID:reset(soft)
    if soft then
        self.integral = self.integral - (self.integral * self.kIOut)
    else
        self.integral = 0
    end

    return self
end

function PID:setLockIntegral(lockIntegral)
    self.lockIntegral = lockIntegral or false

    return self
end

function PID:printDebug()
	pidDebugger.printPid(self.pFactor, self.iFactor, self.dFactor)
end

M.PID = PID

return M