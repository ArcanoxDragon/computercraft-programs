local tArgs = { ... }
if #tArgs > 3 or #tArgs < 1 then
	print( "Usage: excavate <diameter> [startdepth] [height]" )
	return
end

-- Mine in a quarry pattern until we hit something we can't dig
local size = tonumber( tArgs[1] )
if size < 1 then
	print( "Excavate diameter must be positive" )
	return
end

local VALID_FUEL = { "minecraft:coal", "minecraft:blaze_rod", "minecraft:charcoal" }
	
local depth = 0
local unloaded = 0
local collected = 0

local xPos,zPos = 0,0
local xDir,zDir = 0,1

local goTo -- Filled in further down
local refuel -- Filled in further down

local moveSuccess = false
local moveResult = nil
local printedMoveMessage = false
local moveTries = 0
local reseal = false
local done = false
local toDepth = 0

local function isFuel(item)
	for i = 1, #VALID_FUEL do
		if item.name == VALID_FUEL[i] then
			return true
		end
	end
	
	return false
end

local function unload()
	print( "Unloading items..." )
	for n = 1, 16 do
		unloaded = unloaded + turtle.getItemCount(n)
		turtle.select(n)
		local item = turtle.getItemDetail(n)
		if item ~= nil then
			if isFuel(item) then
				turtle.refuel(turtle.getItemCount(n))
			else
				if not turtle.drop() then
					print( "Unable to unload items into inventory! Please make room and press SPACE." )
					local good = false
					repeat
						local e, p1 = os.pullEvent()
						if e == "key" and p1 == 57 then
							if turtle.drop() then
								good = true
							else
								print( "There is still not enough room! Please make room in the unload inventory and press SPACE." )
							end
						end
					until good
				end
			end
		end
	end
	collected = 0
	turtle.select(1)
end

local function returnSupplies()
	local x, y, z, xd, zd = xPos, depth, zPos, xDir, zDir
	print( "Returning to surface..." )
	goTo( 0, y, 0, 0, -1 )
	goTo( 0, 0, 0, 0, -1 )
	
	local fuelNeeded = (x + y + z) * 2 + 4
	if not refuel( fuelNeeded ) then
		unload()
		print( "Waiting for fuel" )
		while turtle.getFuelLevel() < fuelNeeded do
			for n = 1, 16 do
				unloaded = unloaded + turtle.getItemCount(n)
				turtle.select(n)
				local item = turtle.getItemDetail(n)
				if (item ~= nil and isFuel(item)) then
					turtle.refuel(turtle.getItemCount(n))
				end
			end
		end
	else
		unload()	
	end
	
	print( "Resuming mining..." )
	goTo( 0,y,0,xd,zd )
	goTo( x,y,z,xd,zd )
end

local function collect()	
	local bFull = true
	local nTotalItems = 0
	for n=1,16 do
		local nCount = turtle.getItemCount(n)
		if nCount == 0 then
			bFull = false
		end
		nTotalItems = nTotalItems + nCount
	end
	
	if nTotalItems > collected then
		collected = nTotalItems
		if math.fmod(collected + unloaded, 50) == 0 then
			print( "Mined "..(collected + unloaded).." items." )
		end
	end
	
	if bFull then
		print( "No empty slots left." )
		return false
	end
	return true
end

function refuel( amount )
	local fuelLevel = turtle.getFuelLevel()
	if fuelLevel == "unlimited" then
		return true
	end
	
	local needed = amount or math.abs(xPos + zPos + depth + 4)
	if turtle.getFuelLevel() < needed then
		for n=1,16 do
			if turtle.getItemCount(n) > 0 then
				turtle.select(n)
				if turtle.refuel(1) then
					while turtle.getItemCount(n) > 0 and turtle.getFuelLevel() < needed do
						turtle.refuel(1)
					end
					if turtle.getFuelLevel() >= needed then
						turtle.select(1)
						return true
					end
				end
			end
		end
		turtle.select(1)
		return false
	end
	
	return true
end

local function doFwd()
	moveSuccess, moveResult = turtle.forward()
	return moveSuccess, moveResult
end

local function doDn()
	moveSuccess, moveResult = turtle.down()
	return moveSuccess, moveResult
end

local function checkQuitKey()
	if done then return end
	os.startTimer(0.25)
	local e, a1
	while e ~= "char" and e ~= "timer" do e, a1 = os.pullEvent() end
	if e == "char" and a1:lower() == "q" then
		print("Terminating and returning home...")
		done = true
	end
end

local function handleMoveResult()
	if moveSuccess then
		moveTries = 0
		printedMoveMessage = false
	else
		moveTries = moveTries + 1
		
		if moveTries > 5 and not printedMoveMessage then
			print("Can't move: " .. moveResult)
			printedMoveMessage = true
		end
	end
end

local function moveForward()
	parallel.waitForAll(doFwd, checkQuitKey)
	
	handleMoveResult()
	
	return moveSuccess
end

local function moveDown()
	parallel.waitForAll(doDn, checkQuitKey)
	
	handleMoveResult()
	
	return moveSuccess
end

local function tryForwards()
	if not refuel() then
		print( "Not enough Fuel" )
		returnSupplies()
	end
	
	while not moveForward() do
		if turtle.detect() then
			if turtle.dig() then
				if not collect() then
					returnSupplies()
				end
			else
				return false
			end
		elseif turtle.attack() then
			if not collect() then
				returnSupplies()
			end
		else
			sleep( 0.5 )
		end
	end
	
	xPos = xPos + xDir
	zPos = zPos + zDir
	return true
end

local function tryDown()
	if not refuel() then
		print( "Not enough Fuel" )
		returnSupplies()
	end
	
	while not moveDown() do
		if turtle.detectDown() then
			if turtle.digDown() then
				if not collect() then
					returnSupplies()
				end
			else
				return false
			end
		elseif turtle.attackDown() then
			if not collect() then
				returnSupplies()
			end
		else
			sleep( 0.5 )
		end
	end

	depth = depth + 1
	if math.fmod( depth, 10 ) == 0 then
		print( "Descended "..depth.." metres." )
	end

	return true
end

local function turnLeft()
	turtle.turnLeft()
	xDir, zDir = -zDir, xDir
end

local function turnRight()
	turtle.turnRight()
	xDir, zDir = zDir, -xDir
end

function goTo( x, y, z, xd, zd )
	while depth > y do
		if turtle.up() then
			depth = depth - 1
		elseif turtle.digUp() or turtle.attackUp() then
			collect()
		else
			sleep( 0.5 )
		end
	end

	if xPos > x then
		while xDir ~= -1 do
			turnLeft()
		end
		while xPos > x do
			if not tryForwards() then
				if turtle.dig() or turtle.attack() then
					collect()
				else
					sleep( 0.5 )
				end
			end
		end
	elseif xPos < x then
		while xDir ~= 1 do
			turnLeft()
		end
		while xPos < x do
			if not tryForwards() then
				if turtle.dig() or turtle.attack() then
					collect()
				else
					sleep( 0.5 )
				end
			end
		end
	end
	
	if zPos > z then
		while zDir ~= -1 do
			turnLeft()
		end
		while zPos > z do
			if not tryForwards() then
				if turtle.dig() or turtle.attack() then
					collect()
				else
					sleep( 0.5 )
				end
			end
		end
	elseif zPos < z then
		while zDir ~= 1 do
			turnLeft()
		end
		while zPos < z do
			if not tryForwards() then
				if turtle.dig() or turtle.attack() then
					collect()
				else
					sleep( 0.5 )
				end
			end
		end	
	end
	
	while depth < y do
		if not tryDown() then
			if turtle.digDown() or turtle.attackDown() then
				collect()
			else
				sleep( 0.5 )
			end
		end
	end
	
	while zDir ~= zd or xDir ~= xd do
		turnLeft()
	end
end

if not refuel() then
	print( "Out of Fuel" )
	return
end

print( "Excavating..." )

turtle.select(1)

if turtle.digDown() then
	reseal = true
end

if #tArgs > 1 then
    toDepth = tonumber(tArgs[2])
end

goTo(0, toDepth, 0, 0, 1) -- go to set depth

local maxDepth = 256

if #tArgs > 2 then
    maxDepth = tonumber(tArgs[3]) + toDepth
end

local alternate = 0
while not done do
	for n=1,size do
		for m=1,size-1 do
			if not tryForwards() then
				done = true
				break
			end
			
			if done then break end
		end
		
		if done then break end
		if n<size then
			if math.fmod(n + alternate,2) == 0 then
				turnLeft()
				if not tryForwards() then
					done = true
					break
				end
				turnLeft()
			else
				turnRight()
				if not tryForwards() then
					done = true
					break
				end
				turnRight()
			end
		end
		
		
		if done then break end
	end
	
	if done then break end
	
	if size > 1 then
		if math.fmod(size,2) == 0 then
			turnRight()
		else
			if alternate == 0 then
				turnLeft()
			else
				turnRight()
			end
			alternate = 1 - alternate
		end
	end
    
    if depth + 1 >= maxDepth then --if we're going to be moving past the desired depth, break
        done = true
        break
    end
	
	if not tryDown() then
		done = true
		break
	end
end

print( "Returning to surface..." )

-- Return to where we started
goTo( 0,0,0,0,-1 )
unload()
goTo( 0,0,0,0,1 )

-- Seal the hole
if reseal then
	turtle.placeDown()
end

print( "Mined "..(collected + unloaded).." items total." )