local M = {}

local Config = {}

local function trim(str)
	return str:match "^%s*(.-)%s*$"
end

function Config.new(fileName)
	local cfg = {}
	cfg.fileName = fileName
	cfg.keys = {}
	if fs.exists(fileName) then
		local file = fs.open(fileName, "r")
		local text = file.readAll()
		file.close()
		for k, v in string.gmatch(text, "([^=\n]+)=([^=\n]+)") do
			local key = trim(k)
			local value = trim(v)
			local isComment = string.match(k, "^[#].+")

			if not isComment then
				if tonumber(value) then
					cfg.keys[key] = tonumber(value)
				elseif value:lower() == "true" or value:lower() == "false" then
					cfg.keys[key] = (value:lower() == "true")
				else
					cfg.keys[key] = value
				end
			end
		end
	end
	local _cfgMeta = { __index = Config.getKey, __newindex = Config.setKey, __next = Config.next, __pairs = Config.pairs }
	cfg.values = Config.values
	cfg.save = Config.save
	setmetatable(cfg, _cfgMeta)
	return cfg
end

function Config.getKey(config, key)
	return config.keys[key]
end

function Config.setKey(config, key, value)
	config.keys[key] = value
end

function Config.next(config, i)
	return next(config.keys, i)
end

function Config.pairs(config)
	return next, config.keys, nil
end

function Config.values(config)
	return pairs(config.keys)
end

function Config.save(config)
	if fs.exists(config.fileName) then fs.delete(config.fileName) end
	local file = fs.open(config.fileName, "w")
	for k, v in config:values() do
		file.write(k .. "=" .. tostring(v) .. "\n")
	end
	file.close()
end

function M.open(fileName, defaultNew)
	local cfg = Config.new(fileName)
	if not fs.exists(fileName) then
		if defaultNew and type(defaultNew) == "table" then
			for k, v in pairs(defaultNew) do
				cfg[k] = v
			end
			cfg:save()
		end
	end
	return cfg
end

return M